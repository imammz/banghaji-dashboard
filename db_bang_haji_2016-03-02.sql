# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 103.30.247.40 (MySQL 5.6.25-0ubuntu0.15.04.1)
# Database: db_bang_haji
# Generation Time: 2016-03-01 19:54:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(12) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `nama_lengkap` varchar(45) DEFAULT NULL,
  `alamat` varchar(128) DEFAULT NULL,
  `no_hp` varchar(16) DEFAULT NULL,
  `role_id` int(12) DEFAULT NULL,
  `travel_agent_id` int(12) DEFAULT NULL,
  `ref_jenis_kelamin_id` enum('L','P') DEFAULT NULL,
  `gcm` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `path_images` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `email_UNIQUE` (`email`) USING BTREE,
  KEY `fk_admin_role1_idx` (`role_id`) USING BTREE,
  KEY `fk_admin_travel_agent1_idx` (`travel_agent_id`) USING BTREE,
  KEY `fk_admin_ref_jenis_kelamin1_idx` (`ref_jenis_kelamin_id`) USING BTREE,
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`ref_jenis_kelamin_id`) REFERENCES `ref_jenis_kelamin` (`ref_jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admin_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admin_ibfk_3` FOREIGN KEY (`travel_agent_id`) REFERENCES `travel_agent` (`travel_agent_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`admin_id`, `email`, `password`, `nama_lengkap`, `alamat`, `no_hp`, `role_id`, `travel_agent_id`, `ref_jenis_kelamin_id`, `gcm`, `created_at`, `updated_at`, `path_images`)
VALUES
	(1,'koord@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','koord test','Indonesia','02112121212',NULL,1,'L',NULL,'2015-09-21 14:30:42','2015-09-21 14:30:42','images.jpg'),
	(2,'ustadz1@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','Amin Richman','Indonesia','02112121212',4,1,'L',NULL,NULL,NULL,'images.jpg'),
	(3,'ustadz2@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','Yayan Rukhian','Indonesia','02112121212',4,1,'L',NULL,NULL,NULL,'images.jpg'),
	(4,'ustadz3@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','Ustadz 3','Indonesia','02112121212',4,2,'L',NULL,NULL,NULL,'images.jpg'),
	(5,'ustadz4@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','Ustadz 4','Indonesia','02112121212',4,2,'L',NULL,NULL,NULL,'images.jpg'),
	(6,'ustadz5@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','Ustadz 5','Indonesia','02112121212',4,3,'L',NULL,NULL,NULL,'images.jpg'),
	(7,'ustadz6@email.com','$2y$10$nkTOYls.fACBFNiDnERCjuOAWHz/j8KWXNZe4QsPMZA5U/gI5.oOy','Ustadz 6','Indonesia','02112121212',4,3,'L',NULL,NULL,NULL,'images.jpg');

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table anggota_paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `anggota_paket`;

CREATE TABLE `anggota_paket` (
  `anggota_paket_id` int(12) NOT NULL AUTO_INCREMENT,
  `bang_haji_code` varchar(45) DEFAULT NULL,
  `paket_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `pemesanan_id` int(12) DEFAULT NULL,
  `status_lunas` enum('0','1') NOT NULL DEFAULT '0',
  `status_berangkat` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) NOT NULL,
  `status_sosmed` enum('0','1') DEFAULT '0',
  `status_verifikasi` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`anggota_paket_id`,`updated_by`),
  KEY `fk_daftar_paket_umroh_paket_umroh1_idx` (`paket_id`) USING BTREE,
  KEY `fk_daftar_paket_umroh_user1_idx` (`user_id`) USING BTREE,
  KEY `anggota_paket_id` (`anggota_paket_id`),
  CONSTRAINT `anggota_paket_ibfk_1` FOREIGN KEY (`paket_id`) REFERENCES `paket` (`paket_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `anggota_paket_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `anggota_paket` WRITE;
/*!40000 ALTER TABLE `anggota_paket` DISABLE KEYS */;

INSERT INTO `anggota_paket` (`anggota_paket_id`, `bang_haji_code`, `paket_id`, `user_id`, `pemesanan_id`, `status_lunas`, `status_berangkat`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status_sosmed`, `status_verifikasi`)
VALUES
	(1,'BHCD123',1,3,1,'1','1',NULL,NULL,'2015-12-09 23:23:01','','1','1'),
	(2,'BHCD123',1,26,1,'1','1',NULL,NULL,NULL,'','1','1'),
	(3,'BHCD123',1,27,1,'1','1',NULL,NULL,NULL,'','1','1'),
	(4,'BHCD123',1,4,1,'1','1',NULL,NULL,NULL,'','1','1'),
	(5,'BHCD123',1,5,1,'1','1',NULL,NULL,NULL,'','1','1'),
	(6,'BHCD123',1,6,1,'1','1',NULL,NULL,NULL,'','1','1'),
	(7,'BHCD123',1,7,1,'1','1',NULL,NULL,NULL,'','1','1'),
	(11,'BHCD123',1,33,5,'1','0','2016-01-28 17:03:24',NULL,'2016-01-28 17:06:20','','1','1'),
	(12,'BHCD123',2,34,6,'1','0','2016-01-28 17:33:59',NULL,'2016-01-28 17:35:53','','1','1');

/*!40000 ALTER TABLE `anggota_paket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_detail`;

CREATE TABLE `content_detail` (
  `content_detail_id` int(12) NOT NULL AUTO_INCREMENT,
  `order` int(4) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `content_umroh_id` int(11) NOT NULL,
  `ref_filetype_id` int(11) NOT NULL,
  `judul` varchar(45) DEFAULT NULL,
  `content_detail` text,
  `content_detail_arabic` text,
  `content_detail_latin` text,
  `url_video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`content_detail_id`),
  KEY `fk_content_file_content_umroh1_idx` (`content_umroh_id`) USING BTREE,
  KEY `fk_content_file_ref_filetype1_idx` (`ref_filetype_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `content_detail` WRITE;
/*!40000 ALTER TABLE `content_detail` DISABLE KEYS */;

INSERT INTO `content_detail` (`content_detail_id`, `order`, `filename`, `content_umroh_id`, `ref_filetype_id`, `judul`, `content_detail`, `content_detail_arabic`, `content_detail_latin`, `url_video`)
VALUES
	(1,1,'doaihram.mp3',2,6,NULL,'Segala puji bagi Allah yang memberi kami makan dan minum serta menjadikan kami memeluk agama Islam','نَوَيْتُ الْعُمْرَةَ وَاَحْرَمْتُ بِهَا ِللهِ تَعَالَى','Alhamdu lillahhil-ladzi ath-amanaa wa saqaana waja\'alanaa muslimiin',NULL),
	(2,1,'doaihram.mp3',3,6,NULL,'Saya niat umrah dan ihram umrah karena Allah Ta’ala.','نَوَيْتُ الْعُمْرَةَ وَاَحْرَمْتُ بِهَا ِللهِ تَعَالَى','Alhamdu lillahhil-ladzi ath-amanaa wa saqaana waja\'alanaa muslimiin',NULL),
	(3,1,'doaihram.mp3',4,6,NULL,'Saya niat umrah dan ihram umrah karena Allah Ta’ala.','نَوَيْتُ الْعُمْرَةَ وَاَحْرَمْتُ بِهَا ِللهِ تَعَالَى','Alhamdu lillahhil-ladzi ath-amanaa wa saqaana waja\'alanaa muslimiin',NULL),
	(4,1,'doaihram.mp3',5,6,NULL,'Saya niat umrah dan ihram umrah karena Allah Ta’ala.','نَوَيْتُ الْعُمْرَةَ وَاَحْرَمْتُ بِهَا ِللهِ تَعَالَى','Alhamdu lillahhil-ladzi ath-amanaa wa saqaana waja\'alanaa muslimiin',NULL),
	(5,1,'doaihram.mp3',6,6,NULL,'Saya niat umrah dan ihram umrah karena Allah Ta’ala.','نَوَيْتُ الْعُمْرَةَ وَاَحْرَمْتُ بِهَا ِللهِ تَعَالَى','Alhamdu lillahhil-ladzi ath-amanaa wa saqaana waja\'alanaa muslimiin',NULL),
	(6,2,'doaihram.mp3',2,6,NULL,'Wakwaaaw','Lalalalalala','Yeyeyeyeye',NULL);

/*!40000 ALTER TABLE `content_detail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content_kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_kategori`;

CREATE TABLE `content_kategori` (
  `content_kategori_id` int(12) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`content_kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

LOCK TABLES `content_kategori` WRITE;
/*!40000 ALTER TABLE `content_kategori` DISABLE KEYS */;

INSERT INTO `content_kategori` (`content_kategori_id`, `kategori`)
VALUES
	(1,'Ceramah/ Tausiyah'),
	(2,'Tips-tips Haji dan Umroh'),
	(3,'Berita Seputar Umroh'),
	(4,'Blogs'),
	(5,'Video'),
	(6,'Audio'),
	(7,'Gallery Haji dan Umroh'),
	(8,'Doa-doa Haji'),
	(9,'Doa-doa Umroh'),
	(10,'Doa-doa Harian');

/*!40000 ALTER TABLE `content_kategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_tags`;

CREATE TABLE `content_tags` (
  `content_tags_id` int(12) NOT NULL AUTO_INCREMENT,
  `content_umroh_id` int(12) NOT NULL,
  `tags_id` int(12) NOT NULL,
  PRIMARY KEY (`content_tags_id`),
  KEY `fk_content_tags_content_umroh1_idx` (`content_umroh_id`) USING BTREE,
  KEY `fk_content_tags_tags1_idx` (`tags_id`) USING BTREE,
  CONSTRAINT `content_tags_ibfk_1` FOREIGN KEY (`content_umroh_id`) REFERENCES `content_umroh` (`content_umroh_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_tags_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`tags_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table content_umroh
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_umroh`;

CREATE TABLE `content_umroh` (
  `content_umroh_id` int(12) NOT NULL AUTO_INCREMENT,
  `judul` varchar(45) DEFAULT NULL,
  `content` text,
  `content_arabic` text,
  `content_latin` text,
  `content_kategori_id` int(12) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `location` varchar(128) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  `file_image` varchar(128) DEFAULT NULL,
  `admin_id` int(12) NOT NULL,
  `url_video` varchar(128) DEFAULT NULL,
  `file_audio` varchar(128) DEFAULT NULL,
  `duration` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`content_umroh_id`),
  KEY `fk_tausiyah_umroh_admin1_idx` (`created_by`) USING BTREE,
  KEY `fk_content_umroh_content_kategori1_idx` (`content_kategori_id`) USING BTREE,
  CONSTRAINT `content_umroh_ibfk_1` FOREIGN KEY (`content_kategori_id`) REFERENCES `content_kategori` (`content_kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_umroh_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `admin` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `content_umroh` WRITE;
/*!40000 ALTER TABLE `content_umroh` DISABLE KEYS */;

INSERT INTO `content_umroh` (`content_umroh_id`, `judul`, `content`, `content_arabic`, `content_latin`, `content_kategori_id`, `tanggal`, `author`, `location`, `latitude`, `longitude`, `created_at`, `created_by`, `updated_at`, `updated_by`, `file_image`, `admin_id`, `url_video`, `file_audio`, `duration`)
VALUES
	(2,'Doa Ketika Minum Air Zam-Zam',NULL,NULL,NULL,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(3,'Doa makan',NULL,NULL,NULL,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(4,'doa keluar rumah',NULL,NULL,NULL,9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(5,'doa naik kendaraan',NULL,NULL,NULL,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(6,'Doa Melempar Jumroh',NULL,NULL,NULL,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(7,'Hakikat Haji dan Umroh','  ......',NULL,NULL,1,'2015-04-01 00:00:00','Kh Imam Mudzakkir',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'tb_Hindu_Sister_Accept_Islam.png',1,NULL,'Ceramah%20Agama%20oleh%20K.H.%20Zainuddin%20MZ%20(Alm)%20-%20Mencari%20Jodoh%20Yang%20Baik.mp3',100),
	(8,'coba','  ',NULL,NULL,2,'0000-00-00 00:00:00','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(9,'blogs','  ',NULL,NULL,4,'0000-00-00 00:00:00','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(10,'video',NULL,NULL,NULL,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(13,'Rukun Islam ke 5','            ',NULL,NULL,3,'2015-10-25 06:11:00','Ust.Yusuf Mansyur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
	(14,NULL,NULL,NULL,NULL,3,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL);

/*!40000 ALTER TABLE `content_umroh` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fasilitas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitas`;

CREATE TABLE `fasilitas` (
  `fasilitas_id` int(12) NOT NULL AUTO_INCREMENT,
  `travel_agent_id` int(12) DEFAULT NULL,
  `fasilitas_name` varchar(45) DEFAULT NULL,
  `fasilitas_desc` text,
  `fasilitas_category_id` int(12) NOT NULL,
  `fasilitas_star` float(2,0) DEFAULT '0',
  `file_image` varchar(255) DEFAULT NULL COMMENT 'logo untuk fasilitas',
  `fasilitas_motto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fasilitas_id`),
  KEY `fk_fasilitas_fasilitas_category1_idx` (`fasilitas_category_id`) USING BTREE,
  CONSTRAINT `fasilitas_ibfk_1` FOREIGN KEY (`fasilitas_category_id`) REFERENCES `fasilitas_category` (`fasilitas_category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

LOCK TABLES `fasilitas` WRITE;
/*!40000 ALTER TABLE `fasilitas` DISABLE KEYS */;

INSERT INTO `fasilitas` (`fasilitas_id`, `travel_agent_id`, `fasilitas_name`, `fasilitas_desc`, `fasilitas_category_id`, `fasilitas_star`, `file_image`, `fasilitas_motto`)
VALUES
	(1,NULL,'Saraya Eman Hotel di Mekkah','Hotel di mekkah yang disediakan adalah Saraya Eman Hotel yang merupakan hotel berbintang 3. Perjalanan dari hotel ke Masjidil Haram dapat ditempuh hanya dalam 4 menit dengan berjalan kaki. Anda bisa mengakses internet dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas Front desk 24 jam, lift, dan AC. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya\'',1,4,NULL,NULL),
	(2,NULL,'Royal Inn Nokhba Hotel di Madinah','Hotel di mekkah yang disediakan adalah Saraya Eman Hotel yang merupakan hotel berbintang 3. Perjalanan dari hotel ke Masjidil Haram dapat ditempuh hanya dalam 4 menit dengan berjalan kaki. Anda bisa mengakses internet dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas Front desk 24 jam, lift, dan AC. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya\'',1,3,NULL,NULL),
	(3,NULL,'Saudi Airlines','Garuda Indonesia (GA/GIA) adalah maskapai penerbangan dari Indonesia. Secara resmi didirikan pada 28 Januari 1949 dengan nama Garuda Indonesian Airways, Garuda adalah maskapai pertama dan tertua di Indonesia, dan dimiliki oleh pemerintah Republik Indonesia. Sebagai maskapai negara, Garuda Indonesia adalah flag carrier dari Indonesia dengan slogan The Airline of Indonesia serta memiliki lambang Burung Garuda dari mitologi Hindu dan Budha. Dalam penerbangan, Garuda memberikan pelayanan full service yang artinya Garuda memberikan fasilitas dan kenyamanan lebih bagi para penumpangnya.',2,0,'http://103.30.247.40/api/content/image/fasilitas/Garuda-banner.png','Pek kepek kepek'),
	(4,NULL,'Tour','Yeyeye Lalala',3,NULL,NULL,NULL),
	(5,NULL,'Transport Guide','Wakwaaaaaw',4,NULL,'http://103.30.247.40/api/content/image/fasilitas/SaudiGulf-Logo.png',NULL),
	(6,NULL,'Ziarah Makam Para Anbiya','',4,0,NULL,NULL);

/*!40000 ALTER TABLE `fasilitas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fasilitas_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitas_category`;

CREATE TABLE `fasilitas_category` (
  `fasilitas_category_id` int(12) NOT NULL AUTO_INCREMENT,
  `fasilitas_category_name` varchar(64) DEFAULT NULL,
  `fasilitas_category_desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fasilitas_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `fasilitas_category` WRITE;
/*!40000 ALTER TABLE `fasilitas_category` DISABLE KEYS */;

INSERT INTO `fasilitas_category` (`fasilitas_category_id`, `fasilitas_category_name`, `fasilitas_category_desc`)
VALUES
	(1,'Hotel','  '),
	(2,'Maskapai',''),
	(3,'Tour','Fasilitas tour   '),
	(4,'Transport and Guide','    Keterangan');

/*!40000 ALTER TABLE `fasilitas_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fasilitas_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitas_detail`;

CREATE TABLE `fasilitas_detail` (
  `fasilitas_detail_id` int(12) NOT NULL AUTO_INCREMENT,
  `fasilitas_id` int(12) DEFAULT NULL,
  `fasilitas_detail_category_id` int(12) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`fasilitas_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

LOCK TABLES `fasilitas_detail` WRITE;
/*!40000 ALTER TABLE `fasilitas_detail` DISABLE KEYS */;

INSERT INTO `fasilitas_detail` (`fasilitas_detail_id`, `fasilitas_id`, `fasilitas_detail_category_id`, `description`)
VALUES
	(1,1,1,'200m dari Masjidil Haram'),
	(2,1,2,'Brankas, Lift, AC'),
	(3,1,3,'Oyeeeaaaah'),
	(4,2,1,'600m dari Masjidil Haram'),
	(5,2,2,'Brankas, Lift, AC'),
	(6,2,3,'Bahasa Inggris, Bahasa Arab'),
	(7,3,1,'Bagasi kabin 7Kg'),
	(8,3,2,'Tersedia toilet untuk penumpang'),
	(9,3,3,'Tersedia makanan dan minuman di dalam pesawat'),
	(10,4,NULL,'The Best'),
	(11,4,NULL,'Rasa Aman'),
	(12,4,NULL,'Semua Dapat ID Card'),
	(13,5,NULL,'Lewat Turki');

/*!40000 ALTER TABLE `fasilitas_detail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fasilitas_detail_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitas_detail_category`;

CREATE TABLE `fasilitas_detail_category` (
  `fasilitas_detail_category_id` int(12) NOT NULL AUTO_INCREMENT,
  `fasilitas_detail_category_name` varchar(64) DEFAULT NULL,
  `fasilitas_detail_category_icon` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`fasilitas_detail_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `fasilitas_detail_category` WRITE;
/*!40000 ALTER TABLE `fasilitas_detail_category` DISABLE KEYS */;

INSERT INTO `fasilitas_detail_category` (`fasilitas_detail_category_id`, `fasilitas_detail_category_name`, `fasilitas_detail_category_icon`)
VALUES
	(1,'Wi-Fi','wifi.png'),
	(2,'Internet Access','internet.png'),
	(3,'AC','ac.png'),
	(4,'Air Panas','air_panas.png'),
	(5,'Best View','best_view.png');

/*!40000 ALTER TABLE `fasilitas_detail_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fasilitas_gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitas_gallery`;

CREATE TABLE `fasilitas_gallery` (
  `fasilitas_gallery_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `file_image` varchar(128) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `fasilitas_id` int(12) NOT NULL,
  `fasilitas_gallery_category_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`fasilitas_gallery_id`),
  KEY `fk_fasilitas_gallery_fasilitas1_idx` (`fasilitas_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `fasilitas_gallery` WRITE;
/*!40000 ALTER TABLE `fasilitas_gallery` DISABLE KEYS */;

INSERT INTO `fasilitas_gallery` (`fasilitas_gallery_id`, `file_image`, `description`, `fasilitas_id`, `fasilitas_gallery_category_id`)
VALUES
	(1,'30000001000145025_dh.jpg',NULL,1,1),
	(2,'30000001000145032_dh.jpg',NULL,1,1),
	(3,'30000001000145028_dh.jpg',NULL,1,2),
	(4,'30000001000145030_dh.jpg',NULL,1,2),
	(5,'30000001000145025_dh.jpg',NULL,2,1),
	(6,'30000001000145032_dh.jpg',NULL,2,1),
	(7,'30000001000145028_dh.jpg',NULL,2,2),
	(8,'30000001000145030_dh.jpg',NULL,2,2),
	(10,'Garuda-Indonesia-Airbus-A330-243-GA.jpg',NULL,3,2),
	(11,'Garuda-Indonesia-Boeing-777-3Q8-GA.jpg',NULL,3,2),
	(13,'dscn5928.jpg',NULL,4,NULL),
	(14,'thumbs_dsc_0738-17.jpg',NULL,4,NULL),
	(15,'thumbs_dsc_0738-17.jpg',NULL,4,NULL);

/*!40000 ALTER TABLE `fasilitas_gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fasilitas_transit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitas_transit`;

CREATE TABLE `fasilitas_transit` (
  `fasilitas_transit_id` int(12) NOT NULL AUTO_INCREMENT,
  `fasilitas_id` int(12) DEFAULT NULL,
  `fasilitas_transit_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`fasilitas_transit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `fasilitas_transit` WRITE;
/*!40000 ALTER TABLE `fasilitas_transit` DISABLE KEYS */;

INSERT INTO `fasilitas_transit` (`fasilitas_transit_id`, `fasilitas_id`, `fasilitas_transit_name`)
VALUES
	(1,3,'Jakarta'),
	(2,3,'Brunei'),
	(3,3,'Suriah'),
	(4,3,'Iraq'),
	(5,3,'Makkah');

/*!40000 ALTER TABLE `fasilitas_transit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `group_id` int(12) NOT NULL AUTO_INCREMENT,
  `group_nama` varchar(45) DEFAULT NULL,
  `koordinator_id` int(12) DEFAULT NULL,
  `paket_id` int(12) DEFAULT NULL,
  `firebase_url` varchar(255) DEFAULT NULL,
  `geofire_url` varchar(255) DEFAULT NULL,
  `status_aktif` enum('0','1') DEFAULT NULL COMMENT 'Triger''nya berdasarkan tanggal',
  `tanggal_mulai` timestamp NULL DEFAULT NULL,
  `tanggal_akhir` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  CONSTRAINT `group_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `paket` (`paket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;

INSERT INTO `group` (`group_id`, `group_nama`, `koordinator_id`, `paket_id`, `firebase_url`, `geofire_url`, `status_aktif`, `tanggal_mulai`, `tanggal_akhir`)
VALUES
	(1,'JET Varokah',1,1,'group_chat/1','_geofire/1','1','2016-01-01 13:03:06','2016-02-29 13:03:16');

/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_alert
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_alert`;

CREATE TABLE `group_alert` (
  `group_alert_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_alert_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

LOCK TABLES `group_alert` WRITE;
/*!40000 ALTER TABLE `group_alert` DISABLE KEYS */;

INSERT INTO `group_alert` (`group_alert_id`, `user_id`, `group_id`, `lokasi`, `latitude`, `longitude`, `image_path`, `keterangan`, `created_at`)
VALUES
	(1,3,1,'Tangerang Selatan','-6.3487159','106.7529877','1548.1460.2015-11-24_.jpg','Temukan Aku, Pleaaaseeee','2015-12-13 00:11:55'),
	(9,3,1,'Kota Tangerang Selatan','-6.3486159\r\n','106.75299',NULL,NULL,'2016-02-02 11:37:31'),
	(10,3,1,'Kota Tangerang Selatan','-6.3485815\r\n','106.7529756',NULL,NULL,'2016-02-02 13:27:08'),
	(11,3,1,'Kota Tangerang Selatan','-6.3486161\r\n','106.75299',NULL,NULL,'2016-02-02 13:30:07'),
	(12,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 13:34:50'),
	(13,3,1,'Lokasi nya disini','103.12123','0.1231231','alert-13.jpg','keterangaaaaaan','2016-02-02 13:36:12'),
	(14,3,1,'Lokasi nya disini','103.12123','0.1231231','alert-14.jpg','keterangaaaaaan','2016-02-02 13:36:50'),
	(15,3,1,'Lokasi nya disini','103.12123','0.1231231','alert-15.jpg','keterangaaaaaan','2016-02-02 13:37:23'),
	(16,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 13:40:35'),
	(17,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 13:47:49'),
	(18,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 13:57:07'),
	(19,3,1,'Jalan Cabe V, Kota Tangerang Selatan','-6.3485938\r\n','106.7529809',NULL,NULL,'2016-02-02 14:52:41'),
	(20,3,1,'Jalan Cabe V, Kota Tangerang Selatan','-6.3485938\r\n','106.7529809',NULL,NULL,'2016-02-02 14:53:02'),
	(21,3,1,'Kota Tangerang Selatan','-6.3485935\r\n','106.7529807',NULL,NULL,'2016-02-02 14:56:18'),
	(22,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:37:26'),
	(23,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:38:05'),
	(24,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:39:18'),
	(25,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:45:13'),
	(26,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:59:30'),
	(27,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:59:36'),
	(28,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:59:40'),
	(29,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:59:41'),
	(30,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 21:59:43'),
	(31,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:00:06'),
	(32,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:27:02'),
	(33,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:27:22'),
	(34,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:27:24'),
	(35,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:27:25'),
	(36,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:33:57'),
	(37,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:34:08'),
	(38,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:34:09'),
	(39,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:34:10'),
	(40,3,1,'kota tangerenga','374923','323863',NULL,NULL,'2016-02-02 22:34:12'),
	(41,7,1,'Kota Tangerang Selatan','-6.3526031','106.7173153','alert-41.jpg',NULL,'2016-02-03 10:30:15'),
	(42,5,1,'Kota Tangerang Selatan','-6.3049773','106.7568219',NULL,NULL,'2016-02-08 21:33:11'),
	(43,3,1,'Jalan Cabe V, Kota Tangerang Selatan','-6.3487277','106.7530117',NULL,NULL,'2016-02-11 18:13:08'),
	(44,7,1,'Kota Tangerang Selatan','-6.3543526','106.7151712','alert-44.jpg',NULL,'2016-02-12 08:32:02'),
	(45,5,1,'Kota Jakarta Timur','-6.1913843','106.9009443',NULL,NULL,'2016-02-13 07:46:22'),
	(46,3,1,'Kota Tangerang Selatan','-6.3487276','106.7530113',NULL,NULL,'2016-02-15 09:34:43'),
	(47,5,1,'Kota Bekasi','-6.29678','106.9309482',NULL,NULL,'2016-02-20 22:20:59');

/*!40000 ALTER TABLE `group_alert` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_ceramah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_ceramah`;

CREATE TABLE `group_ceramah` (
  `group_ceramah_id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `group_id` int(12) NOT NULL,
  `narasumber` varchar(100) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`group_ceramah_id`),
  KEY `fk_group_ceramah_paket1_idx` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `group_ceramah` WRITE;
/*!40000 ALTER TABLE `group_ceramah` DISABLE KEYS */;

INSERT INTO `group_ceramah` (`group_ceramah_id`, `judul`, `description`, `tanggal`, `group_id`, `narasumber`, `admin_id`, `created_by`, `created_at`)
VALUES
	(25,'TestCeramah','alskjdalskdjalksjd','1970-01-01',1,NULL,1,NULL,NULL),
	(26,'test 1','wewewew\n','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 04:20:26'),
	(27,'test lagi','okey\n','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 04:21:53'),
	(28,'uwow','ahey\n','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:29:09'),
	(29,'aa','dd','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:29:58'),
	(30,'xx\n','dd','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:30:22'),
	(31,'1','q','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:31:24'),
	(32,'w','e','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:32:58'),
	(33,'q','s','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:33:45'),
	(34,'ss','ews','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:34:54'),
	(35,'ddd','ssssd','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:35:47'),
	(36,'d','x.','1970-01-01',1,'Ustadz',NULL,1,'2016-01-31 10:37:09'),
	(37,'tes','tes\n','2016-01-31',1,'Ustadz',NULL,1,'2016-01-31 11:50:47'),
	(38,'f','f','2016-01-31',1,'Ustadz',NULL,1,'2016-01-31 11:51:37'),
	(39,'c','g\n','2016-01-31',1,'Ustadz',NULL,1,'2016-01-31 12:24:43'),
	(40,'ceramah gaul','ustd oke','2016-01-31',1,'Ustadz',NULL,1,'2016-01-31 19:33:20'),
	(41,'ttt','tes','2016-01-31',1,'Ustadz',NULL,1,'2016-01-31 19:38:36'),
	(43,'Ceramah Terbaik 2016','mantap','2016-01-31',1,'Ustadz',NULL,1,'2016-01-31 20:10:50'),
	(44,'Ceramah Terbaik ke-2','tapi boong','2016-01-31',1,'Amin Richman',NULL,1,'2016-02-01 06:27:03'),
	(45,'cobq coba','test test','2016-02-02',1,'Yayan Rukhian',NULL,1,'2016-02-02 13:49:21');

/*!40000 ALTER TABLE `group_ceramah` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_ceramah_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_ceramah_file`;

CREATE TABLE `group_ceramah_file` (
  `group_ceramah_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL,
  `group_ceramah_id` int(11) NOT NULL,
  `ref_filetype_id` int(12) NOT NULL,
  PRIMARY KEY (`group_ceramah_file_id`),
  KEY `fk_group_ceramah_file_group_ceramah1_idx` (`group_ceramah_id`) USING BTREE,
  KEY `fk_group_ceramah_file_ref_filetype1_idx` (`ref_filetype_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `group_ceramah_file` WRITE;
/*!40000 ALTER TABLE `group_ceramah_file` DISABLE KEYS */;

INSERT INTO `group_ceramah_file` (`group_ceramah_file_id`, `file_name`, `group_ceramah_id`, `ref_filetype_id`)
VALUES
	(20,'ceramah-25.mp3',25,6),
	(21,'ceramah-26.mp3',26,6),
	(22,'ceramah-27.mp3',27,6),
	(23,'ceramah-28.mp3',28,6),
	(24,'ceramah-29.mp3',29,6),
	(25,'ceramah-30.mp3',30,6),
	(26,'ceramah-31.mp3',31,6),
	(27,'ceramah-32.mp3',32,6),
	(28,'ceramah-33.mp3',33,6),
	(29,'ceramah-34.mp3',34,6),
	(30,'ceramah-35.mp3',35,6),
	(31,'ceramah-36.mp3',36,6),
	(32,'ceramah-37.mp3',37,6),
	(33,'ceramah-38.mp3',38,6),
	(34,'ceramah-39.mp3',39,6),
	(35,'ceramah-40.mp3',40,6),
	(36,'ceramah-41.mp3',41,6),
	(37,'ceramah-42.jpg',42,6),
	(38,'ceramah-43.mp3',43,6),
	(39,'ceramah-44.mp3',44,6),
	(40,'ceramah-45.mp3',45,6);

/*!40000 ALTER TABLE `group_ceramah_file` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_dokumentasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_dokumentasi`;

CREATE TABLE `group_dokumentasi` (
  `group_dokumentasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `ref_filetype_id` int(12) NOT NULL,
  `saved_name` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `file_size` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`group_dokumentasi_id`),
  KEY `fk_group_ceramah_file_group_ceramah1_idx` (`group_id`) USING BTREE,
  KEY `fk_group_ceramah_file_ref_filetype1_idx` (`ref_filetype_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `group_dokumentasi` WRITE;
/*!40000 ALTER TABLE `group_dokumentasi` DISABLE KEYS */;

INSERT INTO `group_dokumentasi` (`group_dokumentasi_id`, `file_name`, `thumbnail`, `group_id`, `ref_filetype_id`, `saved_name`, `extension`, `file_size`, `created_by`, `created_at`, `updated_at`)
VALUES
	(55,'audio_sample.mp3',NULL,1,6,'audio_sample.mp3-55.mp3','mp3',7783615,1,'2016-01-13 04:21:10','2016-01-13 04:21:10'),
	(56,'video_sample.mp4',NULL,1,5,'video_sample.mp4-56.mp4','mp4',822058,1,'2016-01-13 06:30:06','2016-01-13 06:30:06'),
	(57,'video_sample.mp4',NULL,1,5,'video_sample.mp4-57.mp4','mp4',822058,1,'2016-01-13 06:38:35','2016-01-13 06:38:35'),
	(58,'video_sample.mp4',NULL,1,5,'video_sample.mp4-58.mp4','mp4',822058,1,'2016-01-13 06:44:01','2016-01-13 06:44:01'),
	(59,'video_sample.mp4',NULL,1,5,'video_sample.mp4-59.mp4','mp4',822058,1,'2016-01-13 06:45:09','2016-01-13 06:45:09'),
	(60,'video_sample.mp4',NULL,1,5,'video_sample.mp4-60.mp4','mp4',822058,1,'2016-01-13 07:00:59','2016-01-13 07:00:59'),
	(61,'video_sample.mp4',NULL,1,5,'video_sample.mp4-61.mp4','mp4',822058,1,'2016-01-13 07:33:18','2016-01-13 07:33:18'),
	(62,'video_sample.mp4',NULL,1,5,'video_sample.mp4-62.mp4','mp4',822058,1,'2016-01-13 16:28:15','2016-01-13 16:28:15'),
	(63,'video_sample.mp4',NULL,1,5,'video_sample.mp4-63.mp4','mp4',822058,1,'2016-01-13 16:30:26','2016-01-13 16:30:26'),
	(64,'video_sample.mp4',NULL,1,5,'video_sample.mp4-64.mp4','mp4',822058,1,'2016-01-13 16:31:08','2016-01-13 16:31:08'),
	(65,'video_sample.mp4',NULL,1,5,'video_sample.mp4-65.mp4','mp4',822058,1,'2016-01-13 17:16:14','2016-01-13 17:16:14'),
	(66,'video_sample.mp4',NULL,1,5,'video_sample.mp4-66.mp4','mp4',822058,1,'2016-01-13 17:17:58','2016-01-13 17:17:58'),
	(67,'enterauthorizationcode.mp3',NULL,1,6,'enterauthorizationcode.mp3-67.mp3','mp3',8419,1,'2016-01-13 17:25:11','2016-01-13 17:25:12'),
	(68,'enterauthorizationcode.mp3',NULL,1,6,'enterauthorizationcode.mp3-68.mp3','mp3',8419,1,'2016-01-13 17:28:47','2016-01-13 17:28:47'),
	(69,'video_sample.mp4',NULL,1,5,'video_sample.mp4-69.mp4','mp4',822058,1,'2016-01-13 17:29:15','2016-01-13 17:29:15'),
	(70,'video_sample.mp4',NULL,1,5,'video_sample.mp4-70.mp4','mp4',822058,1,'2016-01-13 17:54:35','2016-01-13 17:54:35'),
	(71,'video_sample.mp4',NULL,1,5,'video_sample.mp4-71.mp4','mp4',822058,1,'2016-01-13 17:55:15','2016-01-13 17:55:15'),
	(72,'video_sample.mp4',NULL,1,5,'video_sample.mp4-72.mp4','mp4',822058,1,'2016-01-13 17:58:35','2016-01-13 17:58:35'),
	(73,'video_sample.mp4',NULL,1,5,'video_sample.mp4-73.mp4','mp4',822058,1,'2016-01-13 18:00:26','2016-01-13 18:00:26'),
	(74,'video_sample.mp4',NULL,1,5,'video_sample.mp4-74.mp4','mp4',822058,1,'2016-01-13 18:01:14','2016-01-13 18:01:14'),
	(75,'video_sample.mp4',NULL,1,5,'video_sample.mp4-75.mp4','mp4',822058,1,'2016-01-14 04:02:06','2016-01-14 04:02:06'),
	(76,'video_sample.mp4',NULL,1,5,'video_sample.mp4-76.mp4','mp4',822058,1,'2016-01-14 04:04:29','2016-01-14 04:04:29'),
	(77,'video_sample.mp4',NULL,1,5,'video_sample.mp4-77.mp4','mp4',822058,1,'2016-01-14 04:06:37','2016-01-14 04:06:37'),
	(78,'video_sample.mp4',NULL,1,5,'video_sample.mp4-78.mp4','mp4',822058,1,'2016-01-14 04:08:26','2016-01-14 04:08:26'),
	(79,'video_sample.mp4',NULL,1,5,'video_sample.mp4-79.mp4','mp4',822058,1,'2016-01-14 04:09:35','2016-01-14 04:09:35'),
	(80,'video_sample.mp4',NULL,1,5,'video_sample.mp4-80.mp4','mp4',822058,1,'2016-01-14 04:10:46','2016-01-14 04:10:46'),
	(81,'video_sample.mp4',NULL,1,5,'video_sample.mp4-81.mp4','mp4',822058,1,'2016-01-14 04:11:18','2016-01-14 04:11:18'),
	(82,'video_sample.mp4',NULL,1,5,'video_sample.mp4-82.mp4','mp4',822058,1,'2016-01-14 14:47:34','2016-01-14 14:47:34'),
	(83,'video_sample.mp4',NULL,1,5,'video_sample.mp4-83.mp4','mp4',822058,1,'2016-01-14 14:47:53','2016-01-14 14:47:53'),
	(84,'IMAGE_20160114_0846282025544984.jpg',NULL,1,1,'IMAGE_20160114_0846282025544984.jpg-84.jpg','jpg',3604616,1,'2016-01-14 15:47:43','2016-01-14 15:47:43'),
	(85,'IMAGE_20160114_085351361799319.jpg',NULL,1,1,'IMAGE_20160114_085351361799319.jpg-85.jpg','jpg',3185782,1,'2016-01-14 15:54:55','2016-01-14 15:54:55'),
	(86,'VID_20160114_100546.mp4',NULL,1,5,'VID_20160114_100546.mp4-86.mp4','mp4',4863084,1,'2016-01-14 17:07:30','2016-01-14 17:07:30'),
	(87,'VID_20160114_102943.mp4',NULL,1,5,'VID_20160114_102943.mp4-87.mp4','mp4',8096858,1,'2016-01-14 17:32:46','2016-01-14 17:32:46'),
	(88,'IMAGE_20160114_1053322025544984.jpg',NULL,1,1,'IMAGE_20160114_1053322025544984.jpg-88.jpg','jpg',3256330,1,'2016-01-14 17:54:58','2016-01-14 17:54:58'),
	(89,'IMAGE_20160114_2013362025544984.jpg',NULL,1,1,'IMAGE_20160114_2013362025544984.jpg-89.jpg','jpg',5289661,1,'2016-01-15 03:16:15','2016-01-15 03:16:15'),
	(90,'IMAGE_20160115_1502402025544984.jpg',NULL,1,1,'IMAGE_20160115_1502402025544984.jpg-90.jpg','jpg',4365668,1,'2016-01-15 22:04:18','2016-01-15 22:04:18'),
	(91,'VID_20160115_165557.mp4',NULL,1,5,'VID_20160115_165557.mp4-91.mp4','mp4',5648063,1,'2016-01-15 23:57:49','2016-01-15 23:57:49'),
	(92,'IMAGE_20160115_1824032025544984.jpg',NULL,1,1,'IMAGE_20160115_1824032025544984.jpg-92.jpg','jpg',3105026,1,'2016-01-16 01:25:37','2016-01-16 01:25:37'),
	(93,'VID_20160115_183045.mp4',NULL,1,5,'VID_20160115_183045.mp4-93.mp4','mp4',4233257,1,'2016-01-16 01:31:55','2016-01-16 01:31:55'),
	(94,'IMAGE_20160116_065314-373791998.jpg',NULL,1,1,'IMAGE_20160116_065314-373791998.jpg-94.jpg','jpg',1460007,1,'2016-01-16 13:53:35','2016-01-16 13:53:35'),
	(95,'IMAGE_20160116_120917-1970958968.jpg',NULL,1,1,'IMAGE_20160116_120917-1970958968.jpg-95.jpg','jpg',1783588,1,'2016-01-16 19:10:32','2016-01-16 19:10:32'),
	(96,'IMAGE_20160119_0840212025544984.jpg',NULL,1,1,'IMAGE_20160119_0840212025544984.jpg-96.jpg','jpg',3460905,1,'2016-01-19 15:41:34','2016-01-19 15:41:34'),
	(97,'IMAGE_20160119_0840431655957299.jpg',NULL,1,1,'IMAGE_20160119_0840431655957299.jpg-97.jpg','jpg',4421453,1,'2016-01-19 15:42:39','2016-01-19 15:42:39'),
	(98,'VID_20160121_143442.mp4',NULL,1,5,'VID_20160121_143442.mp4-98.mp4','mp4',536374,1,'2016-01-21 21:35:06','2016-01-21 21:35:06'),
	(99,'V_20160129_031741.mp4',NULL,1,5,'V_20160129_031741.mp4-99.mp4','mp4',16977851,1,'2016-01-29 10:19:41','2016-01-29 10:19:41'),
	(100,'IMAGE_20160129_200803895663273.jpg',NULL,1,1,'IMAGE_20160129_200803895663273.jpg-100.jpg','jpg',7555,1,'2016-01-30 03:10:15','2016-01-30 03:10:15'),
	(101,'IMAGE_20160202_0650569977605.jpg',NULL,1,1,'IMAGE_20160202_0650569977605.jpg-101.jpg','jpg',1343432,1,'2016-02-02 13:51:18','2016-02-02 13:51:18'),
	(102,'VID_20160202_065111.mp4',NULL,1,5,'VID_20160202_065111.mp4-102.mp4','mp4',1018275,1,'2016-02-02 13:51:28','2016-02-02 13:51:28'),
	(103,'IMAGE_20160202_130244-1473505060.jpg',NULL,1,1,'IMAGE_20160202_130244-1473505060.jpg-103.jpg','jpg',2763596,1,'2016-02-02 20:03:45','2016-02-02 20:03:45'),
	(104,'IMAGE_20160202_1303211678075413.jpg',NULL,1,1,'IMAGE_20160202_1303211678075413.jpg-104.jpg','jpg',2705570,1,'2016-02-02 20:04:15','2016-02-02 20:04:15'),
	(105,'IMAGE_20160202_130646-1473505060.jpg',NULL,1,1,'IMAGE_20160202_130646-1473505060.jpg-105.jpg','jpg',2802277,1,'2016-02-02 20:07:24','2016-02-02 20:07:24'),
	(106,'IMAGE_20160202_1306591678075413.jpg',NULL,1,1,'IMAGE_20160202_1306591678075413.jpg-106.jpg','jpg',2916687,1,'2016-02-02 20:07:53','2016-02-02 20:07:53'),
	(107,'IMAGE_20160202_130739-1796036860.jpg',NULL,1,1,'IMAGE_20160202_130739-1796036860.jpg-107.jpg','jpg',2708744,1,'2016-02-02 20:08:25','2016-02-02 20:08:25'),
	(108,'IMAGE_20160203_000359858546114.jpg',NULL,1,1,'IMAGE_20160203_000359858546114.jpg-108.jpg','jpg',1258493,1,'2016-02-03 07:04:30','2016-02-03 07:04:30'),
	(109,'IMAGE_20160203_0004451990661632.jpg',NULL,1,1,'IMAGE_20160203_0004451990661632.jpg-109.jpg','jpg',268110,1,'2016-02-03 07:05:08','2016-02-03 07:05:08');

/*!40000 ALTER TABLE `group_dokumentasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_feedback`;

CREATE TABLE `group_feedback` (
  `group_feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `feedback` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

LOCK TABLES `group_feedback` WRITE;
/*!40000 ALTER TABLE `group_feedback` DISABLE KEYS */;

INSERT INTO `group_feedback` (`group_feedback_id`, `group_id`, `user_id`, `feedback`, `created_at`)
VALUES
	(1,1,3,'yeyeyeye lalala','2015-12-10 00:29:22'),
	(2,1,3,'lelelele','2015-12-10 07:29:22'),
	(3,1,3,'aklsdalskdjaskdja','2015-12-10 07:59:59'),
	(4,1,3,'aklsdalskdjaskdja','2015-12-10 14:23:17'),
	(5,1,3,'ganteng','2015-12-12 23:45:53'),
	(6,1,3,'anjiiir','2015-12-13 00:20:08'),
	(7,1,3,'gnteng','2015-12-13 00:29:01'),
	(8,1,3,'tudtidftid5id5id57d','2015-12-13 00:39:07'),
	(9,1,3,'gheheje','2015-12-13 01:15:55'),
	(10,1,3,'parah ni hotelnya jelek','2015-12-13 10:54:06'),
	(11,1,3,'keren neg','2015-12-16 01:17:19'),
	(12,1,3,'mantap travel ini','2015-12-16 12:56:17'),
	(13,1,3,'oke aja sih','2015-12-26 02:17:13'),
	(14,1,3,'fhfufufufufiydtististis','2016-01-21 00:59:25'),
	(15,1,3,'arab','2016-01-21 01:38:23'),
	(16,1,3,'amin richman guanteng bingit','2016-01-28 01:56:49'),
	(17,1,3,'bis nya mogok','2016-01-28 02:18:39'),
	(18,1,4,'barokah spa','2016-01-29 05:00:48'),
	(19,1,7,'imut','2016-01-29 10:13:21'),
	(20,1,3,'hahahahahaha mogoooooook','2016-02-16 21:05:18');

/*!40000 ALTER TABLE `group_feedback` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_jadwal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_jadwal`;

CREATE TABLE `group_jadwal` (
  `group_jadwal_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `judul_kegiatan` varchar(128) DEFAULT NULL,
  `waktu_mulai` timestamp NULL DEFAULT NULL,
  `waktu_selesai` timestamp NULL DEFAULT NULL,
  `lokasi` varchar(128) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `keterangan_kegiatan` text,
  `report_waktu_mulai` timestamp NULL DEFAULT NULL,
  `report_waktu_selesai` timestamp NULL DEFAULT NULL,
  `keterangan_delay_mulai` varchar(255) DEFAULT NULL,
  `keterangan_delay_selesai` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`group_jadwal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

LOCK TABLES `group_jadwal` WRITE;
/*!40000 ALTER TABLE `group_jadwal` DISABLE KEYS */;

INSERT INTO `group_jadwal` (`group_jadwal_id`, `group_id`, `judul_kegiatan`, `waktu_mulai`, `waktu_selesai`, `lokasi`, `latitude`, `longitude`, `keterangan_kegiatan`, `report_waktu_mulai`, `report_waktu_selesai`, `keterangan_delay_mulai`, `keterangan_delay_selesai`)
VALUES
	(1,1,'thawaf','2015-12-02 23:22:25','2015-12-09 02:22:30',NULL,NULL,NULL,'yeyeyeye lalala','2015-12-13 00:24:51','2015-12-13 00:24:51','sori delay','sori delay'),
	(2,1,'wukuf','2015-12-02 23:38:45','2015-12-10 23:38:49',NULL,NULL,NULL,'lalalalala yeyeyeyeye','2016-01-12 00:06:39','2016-01-12 00:06:49','N/A','N/A'),
	(3,1,'sai','2015-12-03 23:38:45','2015-12-10 23:38:49',NULL,NULL,NULL,'lalalalala yeyeyeyeye','2016-01-12 00:10:38','2016-01-12 00:10:44',NULL,NULL),
	(4,1,'bermalam di mina','2015-12-03 23:38:45','2015-12-10 23:38:49',NULL,NULL,NULL,'lalalalala yeyeyeyeye','2016-01-12 14:31:44','2016-01-12 14:31:45',NULL,NULL),
	(5,1,'masjidil haram','2015-12-06 17:38:45','2015-12-06 19:38:49',NULL,NULL,NULL,'lalalalala yeyeyeyeye','2016-01-12 14:31:57','2016-01-12 14:32:00',NULL,NULL),
	(6,1,'ziarah','2015-12-06 21:38:45','2015-12-06 22:38:49',NULL,NULL,NULL,'Setelah sholat Subuh sarapan, jamaah akan mulai berziarah ke Makam Rasulullah / Raudhah, yang berada di lokasi Masjid Nabawi di Madinah. Berziarah di Masjid yaitu Raudhah. Bagi mereka yang berdoa di Raudhah maka Allah akan mendengarkan doa itu, dan mengabulkannya.','2016-01-15 20:22:06',NULL,NULL,NULL),
	(7,1,'bebas','2015-12-07 13:38:45','2015-12-07 14:38:49',NULL,NULL,NULL,'lalalalala yeyeyeyeye','2016-01-30 05:15:07','2016-01-30 05:15:08',NULL,NULL),
	(8,1,'test','2015-12-07 13:38:45','2015-12-07 14:38:49',NULL,NULL,NULL,'lalalalala yeyeyeyeye',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `group_jadwal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_member`;

CREATE TABLE `group_member` (
  `group_member_id` int(12) NOT NULL AUTO_INCREMENT,
  `group_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `anggota_paket_id` int(12) DEFAULT NULL,
  `kode_member` varchar(45) DEFAULT NULL COMMENT 'Kode peserta dari travel agent',
  `marker_xhdpi` varchar(255) DEFAULT NULL,
  `marker_xxxhdpi` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_member_id`),
  KEY `fk_group_member_group1_idx` (`group_id`) USING BTREE,
  KEY `fk_group_member_user1_idx` (`user_id`) USING BTREE,
  KEY `anggota_paket_id` (`anggota_paket_id`),
  CONSTRAINT `group_member_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_member_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_member_ibfk_3` FOREIGN KEY (`anggota_paket_id`) REFERENCES `anggota_paket` (`anggota_paket_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `group_member` WRITE;
/*!40000 ALTER TABLE `group_member` DISABLE KEYS */;

INSERT INTO `group_member` (`group_member_id`, `group_id`, `user_id`, `anggota_paket_id`, `kode_member`, `marker_xhdpi`, `marker_xxxhdpi`, `created_at`, `updated_at`)
VALUES
	(1,1,3,1,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(2,1,28,1,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(3,1,29,1,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(4,1,30,1,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(5,1,26,2,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(6,1,27,3,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(7,1,4,4,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(8,1,5,5,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(9,1,6,6,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(10,1,7,7,NULL,'marker_xhdpi.png','marker_xxxhdpi.png',NULL,NULL),
	(11,1,33,11,NULL,'marker_xhdpi.png','marker_xxxhdpi.png','2016-01-28 17:14:34','2016-01-28 17:14:34'),
	(12,1,34,12,NULL,'marker_xhdpi.png','marker_xxxhdpi.png','2016-01-28 17:35:53','2016-01-28 17:35:53');

/*!40000 ALTER TABLE `group_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table haji
# ------------------------------------------------------------

DROP TABLE IF EXISTS `haji`;

CREATE TABLE `haji` (
  `haji_id` int(12) NOT NULL AUTO_INCREMENT,
  `tahun_masehi` varchar(4) DEFAULT NULL,
  `tahun_hijriah` varchar(45) DEFAULT NULL,
  `kloter` int(11) DEFAULT NULL,
  PRIMARY KEY (`haji_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `haji` WRITE;
/*!40000 ALTER TABLE `haji` DISABLE KEYS */;

INSERT INTO `haji` (`haji_id`, `tahun_masehi`, `tahun_hijriah`, `kloter`)
VALUES
	(1,'2015','1436',1);

/*!40000 ALTER TABLE `haji` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jadwal_paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jadwal_paket`;

CREATE TABLE `jadwal_paket` (
  `jadwal_paket_id` int(12) NOT NULL AUTO_INCREMENT,
  `ref_kegiatan_id` int(12) NOT NULL,
  `waktu_mulai` datetime DEFAULT NULL,
  `waktu_selesai` datetime DEFAULT NULL,
  `waktu_mulai_real` datetime DEFAULT NULL,
  `waktu_selesai_real` datetime DEFAULT NULL,
  `lokasi_kegiatan` varchar(45) DEFAULT NULL,
  `keterangan_kegiatan` varchar(45) DEFAULT NULL,
  `paket_id` int(12) NOT NULL,
  PRIMARY KEY (`jadwal_paket_id`),
  KEY `fk_jadwal_paket_paket1_idx` (`paket_id`) USING BTREE,
  KEY `fk_jadwal_paket_ref_kegiatan1_idx` (`ref_kegiatan_id`) USING BTREE,
  CONSTRAINT `jadwal_paket_ibfk_1` FOREIGN KEY (`paket_id`) REFERENCES `paket` (`paket_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jadwal_paket_ibfk_2` FOREIGN KEY (`ref_kegiatan_id`) REFERENCES `ref_kegiatan` (`ref_kegiatan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

LOCK TABLES `jadwal_paket` WRITE;
/*!40000 ALTER TABLE `jadwal_paket` DISABLE KEYS */;

INSERT INTO `jadwal_paket` (`jadwal_paket_id`, `ref_kegiatan_id`, `waktu_mulai`, `waktu_selesai`, `waktu_mulai_real`, `waktu_selesai_real`, `lokasi_kegiatan`, `keterangan_kegiatan`, `paket_id`)
VALUES
	(1,2,'2015-11-10 23:45:53','2015-11-10 23:45:58',NULL,NULL,NULL,'Lorem Lorembooo',1),
	(2,3,'2015-11-10 23:49:09','2015-11-10 23:49:14',NULL,NULL,NULL,'Lorem Lorembooo',1),
	(3,4,'2015-11-11 23:49:12','2015-11-11 23:49:18',NULL,NULL,NULL,'Lorem Lorembooo',1),
	(4,2,'2015-11-15 23:45:53','2015-11-15 23:45:58',NULL,NULL,NULL,'Lorem Lorembooo',2),
	(5,3,'2015-11-15 23:49:09','2015-11-15 23:49:14',NULL,NULL,NULL,'Lorem Lorembooo',2),
	(6,4,'2015-11-15 23:49:12','2015-11-15 23:49:18',NULL,NULL,NULL,'Lorem Lorembooo',2),
	(7,2,'2015-11-15 23:45:53','2015-11-15 23:45:58',NULL,NULL,NULL,'Lorem Lorembooo',3),
	(8,3,'2015-11-15 23:49:09','2015-11-15 23:49:14',NULL,NULL,NULL,'Lorem Lorembooo',3),
	(9,4,'2015-11-15 23:49:12','2015-11-15 23:49:18',NULL,NULL,NULL,'Lorem Lorembooo',3);

/*!40000 ALTER TABLE `jadwal_paket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `messages_id` int(12) NOT NULL,
  `content` text,
  `from` int(12) NOT NULL,
  `to` int(12) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`messages_id`),
  KEY `fk_messages_group_member1_idx` (`from`) USING BTREE,
  KEY `fk_messages_group_member2_idx` (`to`) USING BTREE,
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`from`) REFERENCES `group_member` (`group_member_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`to`) REFERENCES `group_member` (`group_member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_04_24_110151_create_oauth_scopes_table',1),
	('2014_04_24_110304_create_oauth_grants_table',1),
	('2014_04_24_110403_create_oauth_grant_scopes_table',1),
	('2014_04_24_110459_create_oauth_clients_table',1),
	('2014_04_24_110557_create_oauth_client_endpoints_table',1),
	('2014_04_24_110705_create_oauth_client_scopes_table',1),
	('2014_04_24_110817_create_oauth_client_grants_table',1),
	('2014_04_24_111002_create_oauth_sessions_table',1),
	('2014_04_24_111109_create_oauth_session_scopes_table',1),
	('2014_04_24_111254_create_oauth_auth_codes_table',1),
	('2014_04_24_111403_create_oauth_auth_code_scopes_table',1),
	('2014_04_24_111518_create_oauth_access_tokens_table',1),
	('2014_04_24_111657_create_oauth_access_token_scopes_table',1),
	('2014_04_24_111810_create_oauth_refresh_tokens_table',1),
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mutasi_paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mutasi_paket`;

CREATE TABLE `mutasi_paket` (
  `confirm_paket_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_paket_id` int(12) NOT NULL,
  `to_paket_id` int(12) NOT NULL,
  `anggota_paket_id` int(12) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`confirm_paket_id`),
  KEY `fk_mutasi_paket_paket1_idx` (`from_paket_id`) USING BTREE,
  KEY `fk_mutasi_paket_paket2_idx` (`to_paket_id`) USING BTREE,
  KEY `fk_mutasi_paket_anggota_paket1_idx` (`anggota_paket_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;



# Dump of table oauth_access_token_scopes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_access_token_scopes`;

CREATE TABLE `oauth_access_token_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`) USING BTREE,
  KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`) USING BTREE,
  CONSTRAINT `oauth_access_token_scopes_ibfk_1` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_access_token_scopes_ibfk_2` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`) USING BTREE,
  KEY `oauth_access_tokens_session_id_index` (`session_id`) USING BTREE,
  CONSTRAINT `oauth_access_tokens_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;

INSERT INTO `oauth_access_tokens` (`id`, `session_id`, `expire_time`, `created_at`, `updated_at`)
VALUES
	('0GlVZJaXAQ16eCAkKRDsqWqw9Xd32PVcelBfdmTh',5,1441871389,'2015-09-10 13:49:49','2015-09-10 13:49:49'),
	('5HRLL77OQv3T5sCjc2hhyDJ6IzXcmprGrgAZII2n',1,1441631349,'2015-09-07 19:09:09','2015-09-07 19:09:09'),
	('NqCYXqPUguzeYUHqKspKNlitnMBJ11Nv2J4QQAM6',3,1441868992,'2015-09-10 13:09:53','2015-09-10 13:09:53'),
	('VoPwxEaoioMhmAlmceQM3CFiFzFukgoRUSueba0g',4,1441871380,'2015-09-10 13:49:41','2015-09-10 13:49:41'),
	('YXvzaPTuBbZOEJTjqbGcZUZKQhoIrQrL7VUabhnP',6,1441874727,'2015-09-10 14:45:28','2015-09-10 14:45:28'),
	('Z8u6qoyDrk875PeidlAOkBvuRYy4yPOUIANOlSF2',2,1441648947,'2015-09-08 00:02:28','2015-09-08 00:02:28');

/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_auth_code_scopes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_auth_code_scopes`;

CREATE TABLE `oauth_auth_code_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth_code_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`) USING BTREE,
  KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`) USING BTREE,
  CONSTRAINT `oauth_auth_code_scopes_ibfk_1` FOREIGN KEY (`auth_code_id`) REFERENCES `oauth_auth_codes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_auth_code_scopes_ibfk_2` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_auth_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_session_id_index` (`session_id`) USING BTREE,
  CONSTRAINT `oauth_auth_codes_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_client_endpoints
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_client_endpoints`;

CREATE TABLE `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`) USING BTREE,
  CONSTRAINT `oauth_client_endpoints_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_client_grants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_client_grants`;

CREATE TABLE `oauth_client_grants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_client_grants_client_id_index` (`client_id`) USING BTREE,
  KEY `oauth_client_grants_grant_id_index` (`grant_id`) USING BTREE,
  CONSTRAINT `oauth_client_grants_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `oauth_client_grants_ibfk_2` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_client_scopes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_client_scopes`;

CREATE TABLE `oauth_client_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_client_scopes_client_id_index` (`client_id`) USING BTREE,
  KEY `oauth_client_scopes_scope_id_index` (`scope_id`) USING BTREE,
  CONSTRAINT `oauth_client_scopes_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_client_scopes_ibfk_2` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;

INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `created_at`, `updated_at`)
VALUES
	('1','12345','gozal','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_grant_scopes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_grant_scopes`;

CREATE TABLE `oauth_grant_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_grant_scopes_grant_id_index` (`grant_id`) USING BTREE,
  KEY `oauth_grant_scopes_scope_id_index` (`scope_id`) USING BTREE,
  CONSTRAINT `oauth_grant_scopes_ibfk_1` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_grant_scopes_ibfk_2` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_grants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_grants`;

CREATE TABLE `oauth_grants` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`access_token_id`),
  UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`) USING BTREE,
  CONSTRAINT `oauth_refresh_tokens_ibfk_1` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_scopes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_scopes`;

CREATE TABLE `oauth_scopes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_session_scopes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_session_scopes`;

CREATE TABLE `oauth_session_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_session_scopes_session_id_index` (`session_id`) USING BTREE,
  KEY `oauth_session_scopes_scope_id_index` (`scope_id`) USING BTREE,
  CONSTRAINT `oauth_session_scopes_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_session_scopes_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauth_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_sessions`;

CREATE TABLE `oauth_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`) USING BTREE,
  CONSTRAINT `oauth_sessions_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `oauth_sessions` WRITE;
/*!40000 ALTER TABLE `oauth_sessions` DISABLE KEYS */;

INSERT INTO `oauth_sessions` (`id`, `client_id`, `owner_type`, `owner_id`, `client_redirect_uri`, `created_at`, `updated_at`)
VALUES
	(1,'1','user','1',NULL,'2015-09-07 19:09:09','2015-09-07 19:09:09'),
	(2,'1','user','1',NULL,'2015-09-08 00:02:27','2015-09-08 00:02:27'),
	(3,'1','user','1',NULL,'2015-09-10 13:09:53','2015-09-10 13:09:53'),
	(4,'1','user','1',NULL,'2015-09-10 13:49:41','2015-09-10 13:49:41'),
	(5,'1','user','1',NULL,'2015-09-10 13:49:49','2015-09-10 13:49:49'),
	(6,'1','user','1',NULL,'2015-09-10 14:45:27','2015-09-10 14:45:27');

/*!40000 ALTER TABLE `oauth_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket`;

CREATE TABLE `paket` (
  `paket_id` int(12) NOT NULL AUTO_INCREMENT,
  `paket_name` varchar(68) DEFAULT NULL,
  `tanggal_berangkat` date DEFAULT NULL,
  `tanggal_pulang` date DEFAULT NULL,
  `harga_total_paket` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `currency` enum('USD','IDR') DEFAULT 'IDR',
  `keterangan` text,
  `travel_agent_id` int(12) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL,
  `deadline_at` timestamp NULL DEFAULT NULL,
  `jenis_paket` enum('HAJI','UMROH') NOT NULL DEFAULT 'UMROH',
  `status_promo` enum('0','1') DEFAULT '0',
  `max_kuota` int(3) DEFAULT NULL,
  `min_kuota` int(11) DEFAULT NULL,
  `sisa_kuota` int(3) DEFAULT NULL,
  `umroh_id` int(11) DEFAULT NULL,
  `haji_id` int(11) DEFAULT NULL,
  `status_paket` enum('0','1') DEFAULT '1',
  `koordinator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`paket_id`),
  KEY `fk_paket_umroh_master_travel_agent1_idx` (`travel_agent_id`) USING BTREE,
  CONSTRAINT `paket_ibfk_1` FOREIGN KEY (`travel_agent_id`) REFERENCES `travel_agent` (`travel_agent_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `paket` WRITE;
/*!40000 ALTER TABLE `paket` DISABLE KEYS */;

INSERT INTO `paket` (`paket_id`, `paket_name`, `tanggal_berangkat`, `tanggal_pulang`, `harga_total_paket`, `discount`, `currency`, `keterangan`, `travel_agent_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deadline_at`, `jenis_paket`, `status_promo`, `max_kuota`, `min_kuota`, `sisa_kuota`, `umroh_id`, `haji_id`, `status_paket`, `koordinator_id`)
VALUES
	(1,'Umroh Reguler Karomah 12 Hari','2015-12-24','2016-01-05',36300000,NULL,'IDR','Maskapai yang digunakan adalah Garuda Indonesia / Saudi Air Lines, maskapai ini dinilai 4 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, Bisnis dan First Class (bergantung ketersediaan).\r\n\r\nHotel di mekkah yang disediakan adalah Dar Al Ghufran Hotel yang merupakan hotel berbintang 5. Hotel ini berlokasi sangat dekat dengan Masjidil Haram. Anda bisa mengakses internet secara gratis dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas area bebas rokok, lift, dan AC. Kami dapat melayani anda dalam bahasa inggris dan arab. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya.\r\n\r\nHotel di madinah yang disediakan adalah Royal Dyar Hotel yang merupakan hotel berbintang 5. Perjalanan dari hotel ke Masjid Nabawi dapat ditempuh hanya dalam 3 menit dengan berjalan kaki. Anda bisa mengakses internet secara gratis dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas area bebas asap rokok, ruang keluarga, lift dan AC. Kami dapat melayani Anda dalam bahasa inggris dan arab. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya.\r\n\r\nPastikan Anda memilih paket umroh dari ANNISA TRAVEL yang menjamin kenyamanan ibadah Anda.',1,'2015-09-13 12:54:14','annisa','2015-09-13 12:54:29','annisa','2015-10-01 11:08:38','UMROH','1',10,5,10,1,NULL,'1',2),
	(2,'Paket Haji 30 Hari','2015-09-02','2015-09-25',48000000,50000,'IDR','Maskapai yang digunakan adalah Garuda Indonesia / Saudi Air Lines, maskapai ini dinilai 4 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, Bisnis dan First Class (bergantung ketersediaan).\r\n\r\nHotel di mekkah yang disediakan adalah Dar Al Ghufran Hotel yang merupakan hotel berbintang 5. Hotel ini berlokasi sangat dekat dengan Masjidil Haram. Anda bisa mengakses internet secara gratis dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas area bebas rokok, lift, dan AC. Kami dapat melayani anda dalam bahasa inggris dan arab. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya.\r\n\r\nHotel di madinah yang disediakan adalah Royal Dyar Hotel yang merupakan hotel berbintang 5. Perjalanan dari hotel ke Masjid Nabawi dapat ditempuh hanya dalam 3 menit dengan berjalan kaki. Anda bisa mengakses internet secara gratis dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas area bebas asap rokok, ruang keluarga, lift dan AC. Kami dapat melayani Anda dalam bahasa inggris dan arab. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya.\r\n\r\nPastikan Anda memilih paket umroh dari ANNISA TRAVEL yang menjamin kenyamanan ibadah Anda.',1,'2015-09-13 12:54:14','annisa','2015-09-13 12:54:29','annisa','2015-10-22 11:08:46','HAJI','1',10,5,10,NULL,1,'1',2),
	(3,'Umrah Ekonomis 14 Hari Start Balikpapan','2015-11-23','2015-12-04',35000000,300000,'IDR','Maskapai yang digunakan adalah Saudi Airlines, maskapai ini dinilai 4 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, kelas Bisnis dan kelas Utama (bergantung ketersediaan). Saudia menyediakan berbagai kenyamanan di dalam penerbangan, seperti: makanan khas timur tengah, salad, minuman panas dan dingin .\r\n\r\nHotel di mekkah yang disediakan adalah Royal Dar Al Eiman yang merupakan hotel berbintang 5. Perjalanan dari hotel ke Masjidil Haram dapat ditempuh hanya dalam 2 menit dengan berjalan kaki. Anda bisa mengakses internet dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas area bebas rokok, lift, dan AC. Kami dapat melayani anda dalam bahasa inggris dan arab. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya.\r\n\r\nRoyal Inn berjarak 50 meter dari Al-Masjid al-Nabawi, dan 10 menit berkendara dari stasiun umum. Hotel ini menawarkan sebuah restoran dengan layanan kamar 24 jam dan pemandangan Masjid Suci.\r\n\r\nKamar-kamar luas di The Royal Inn menyediakan Wi-Fi gratis, dan dilengkapi dengan minibar, AC, serta TV layar datar. Setiap kamar memiliki kamar mandi dalam dengan pengering rambut dan produk mandi pilihan.\r\n\r\nStaf meja depan 24 jam di Royal Inn dapat membantu Anda dengan layanan kamar dan permintaan binatu serta menyetrika. Tempat parkir pribadi gratis juga tersedia.\r\n\r\nBandara Prince Mohamed Bin Abdul Aziz dapat dicapai dalam 20 menit berkendara.\r\n\r\nKamar Hotel: 242 Akomodasi ini sudah ada di Booking.com sejak 4 Mei 2014.',2,'2015-09-22 07:37:04','amsori',NULL,NULL,'2015-10-23 11:08:55','UMROH','1',15,7,15,1,NULL,'1',2),
	(4,'Umrah Plus Istambul bulan Muharam + Safar 1436H','2015-11-08','2015-09-16',45700000,500000,'IDR','Maskapai yang digunakan adalah Saudi Airlines, maskapai ini dinilai 4 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, kelas Bisnis dan kelas Utama (bergantung ketersediaan). Saudia menyediakan berbagai kenyamanan di dalam penerbangan, seperti: makanan khas timur tengah, salad, minuman panas dan dingin .\r\n\r\nHotel di mekkah yang disediakan adalah Royal Dar Al Eiman yang merupakan hotel berbintang 5. Perjalanan dari hotel ke Masjidil Haram dapat ditempuh hanya dalam 2 menit dengan berjalan kaki. Anda bisa mengakses internet dengan menggunakan Wifi di dalam hotel ini. Untuk kenyamanan Anda, kami menyediakan fasilitas area bebas rokok, lift, dan AC. Kami dapat melayani anda dalam bahasa inggris dan arab. Makanan dan minuman tersedia di restoran kami. Tanyakan pada resepsionis atau housekeeping jika memerlukan layanan lainnya.\r\n\r\nRoyal Inn berjarak 50 meter dari Al-Masjid al-Nabawi, dan 10 menit berkendara dari stasiun umum. Hotel ini menawarkan sebuah restoran dengan layanan kamar 24 jam dan pemandangan Masjid Suci.\r\n\r\nKamar-kamar luas di The Royal Inn menyediakan Wi-Fi gratis, dan dilengkapi dengan minibar, AC, serta TV layar datar. Setiap kamar memiliki kamar mandi dalam dengan pengering rambut dan produk mandi pilihan.\r\n\r\nStaf meja depan 24 jam di Royal Inn dapat membantu Anda dengan layanan kamar dan permintaan binatu serta menyetrika. Tempat parkir pribadi gratis juga tersedia.\r\n\r\nBandara Prince Mohamed Bin Abdul Aziz dapat dicapai dalam 20 menit berkendara.\r\n\r\nKamar Hotel: 242 Akomodasi ini sudah ada di Booking.com sejak 4 Mei 2014.',3,'2015-11-08 07:46:38','patuna',NULL,NULL,'2015-10-28 11:08:59','UMROH','1',10,6,9,1,NULL,'1',2);

/*!40000 ALTER TABLE `paket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paket_fasilitas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket_fasilitas`;

CREATE TABLE `paket_fasilitas` (
  `paket_fasilitas_id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) DEFAULT NULL,
  `fasilitas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`paket_fasilitas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

LOCK TABLES `paket_fasilitas` WRITE;
/*!40000 ALTER TABLE `paket_fasilitas` DISABLE KEYS */;

INSERT INTO `paket_fasilitas` (`paket_fasilitas_id`, `paket_id`, `fasilitas_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(4,2,1),
	(5,1,3),
	(6,2,2),
	(7,2,3),
	(8,3,1),
	(9,3,2),
	(10,3,3),
	(14,4,1),
	(15,4,2),
	(16,4,3),
	(17,4,4),
	(23,1,4),
	(24,2,4),
	(25,3,4),
	(26,1,5),
	(27,2,5),
	(28,3,5),
	(29,4,6);

/*!40000 ALTER TABLE `paket_fasilitas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paket_prasyarat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket_prasyarat`;

CREATE TABLE `paket_prasyarat` (
  `paket_prasyarat_id` int(12) NOT NULL AUTO_INCREMENT,
  `paket_id` int(12) NOT NULL,
  `ref_prasyarat_id` int(12) NOT NULL,
  PRIMARY KEY (`paket_prasyarat_id`),
  KEY `fk_paket_prasyarat_paket1_idx` (`paket_id`) USING BTREE,
  KEY `fk_paket_prasyarat_ref_prasyarat1_idx` (`ref_prasyarat_id`) USING BTREE,
  CONSTRAINT `paket_prasyarat_ibfk_1` FOREIGN KEY (`paket_id`) REFERENCES `paket` (`paket_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `paket_prasyarat_ibfk_2` FOREIGN KEY (`ref_prasyarat_id`) REFERENCES `ref_prasyarat` (`ref_prasyarat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

LOCK TABLES `paket_prasyarat` WRITE;
/*!40000 ALTER TABLE `paket_prasyarat` DISABLE KEYS */;

INSERT INTO `paket_prasyarat` (`paket_prasyarat_id`, `paket_id`, `ref_prasyarat_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,1,3),
	(4,2,1),
	(5,2,2),
	(6,2,3),
	(7,3,1),
	(8,3,2),
	(9,3,3),
	(11,4,1),
	(12,4,2),
	(13,4,3);

/*!40000 ALTER TABLE `paket_prasyarat` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table panic_calls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `panic_calls`;

CREATE TABLE `panic_calls` (
  `panic_calls_id` int(12) NOT NULL AUTO_INCREMENT,
  `group_member_id` int(12) NOT NULL,
  `panic_calls_messages` varchar(255) DEFAULT NULL,
  `panic_calls_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`panic_calls_id`),
  KEY `fk_panic_calls_group_member1_idx` (`group_member_id`) USING BTREE,
  CONSTRAINT `panic_calls_ibfk_1` FOREIGN KEY (`group_member_id`) REFERENCES `group_member` (`group_member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`) USING BTREE,
  KEY `password_resets_token_index` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pemesanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pemesanan`;

CREATE TABLE `pemesanan` (
  `pemesanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `jumlah_orang` int(3) DEFAULT NULL,
  `harga_paket` double DEFAULT NULL,
  `harga_potongan` double DEFAULT NULL,
  `harga_dibayar` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pemesanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

LOCK TABLES `pemesanan` WRITE;
/*!40000 ALTER TABLE `pemesanan` DISABLE KEYS */;

INSERT INTO `pemesanan` (`pemesanan_id`, `paket_id`, `user_id`, `jumlah_orang`, `harga_paket`, `harga_potongan`, `harga_dibayar`, `created_at`, `updated_at`)
VALUES
	(1,1,1,2,2000000,0,2000000,'2015-12-06 14:13:27',NULL),
	(2,1,1,1,36300000,36300000,36300000,'2016-01-28 16:04:13','2016-01-28 16:04:13'),
	(3,1,1,1,36300000,36300000,36300000,'2016-01-28 16:04:54','2016-01-28 16:04:54'),
	(4,1,1,1,36300000,36300000,36300000,'2016-01-28 16:04:56','2016-01-28 16:04:56'),
	(5,1,1,1,36300000,36300000,36300000,'2016-01-28 17:03:24','2016-01-28 17:03:24'),
	(6,2,1,1,48000000,48000000,48000000,'2016-01-28 17:33:59','2016-01-28 17:33:59');

/*!40000 ALTER TABLE `pemesanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `places_id` int(12) NOT NULL AUTO_INCREMENT,
  `places_name` varchar(64) DEFAULT NULL,
  `places_desc` text,
  `latitude` varchar(68) DEFAULT NULL,
  `longitude` varchar(68) DEFAULT NULL,
  `path_images` varchar(255) DEFAULT NULL,
  `places_category_id` int(11) NOT NULL,
  PRIMARY KEY (`places_id`),
  KEY `fk_places_places_category1_idx` (`places_category_id`) USING BTREE,
  CONSTRAINT `places_ibfk_1` FOREIGN KEY (`places_category_id`) REFERENCES `places_category` (`places_category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table places_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places_category`;

CREATE TABLE `places_category` (
  `places_category_id` int(12) NOT NULL,
  `places_category_name` varchar(45) DEFAULT NULL,
  `places_category_desc` text,
  PRIMARY KEY (`places_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ref_bank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_bank`;

CREATE TABLE `ref_bank` (
  `ref_bank_id` int(12) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(45) DEFAULT NULL,
  `bank_country` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ref_bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `ref_bank` WRITE;
/*!40000 ALTER TABLE `ref_bank` DISABLE KEYS */;

INSERT INTO `ref_bank` (`ref_bank_id`, `bank_name`, `bank_country`)
VALUES
	(1,'BCA','Indonesia'),
	(2,'BNI','Indonesia');

/*!40000 ALTER TABLE `ref_bank` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_document`;

CREATE TABLE `ref_document` (
  `ref_document_id` int(12) NOT NULL AUTO_INCREMENT,
  `document` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ref_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ref_fasilitas_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_fasilitas_detail`;

CREATE TABLE `ref_fasilitas_detail` (
  `ref_fasilitas_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ref_fasilitas_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;



# Dump of table ref_filetype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_filetype`;

CREATE TABLE `ref_filetype` (
  `ref_filetype_id` int(12) NOT NULL AUTO_INCREMENT,
  `filetype` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ref_filetype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

LOCK TABLES `ref_filetype` WRITE;
/*!40000 ALTER TABLE `ref_filetype` DISABLE KEYS */;

INSERT INTO `ref_filetype` (`ref_filetype_id`, `filetype`)
VALUES
	(1,'image'),
	(2,'document'),
	(3,'spreadsheet'),
	(4,'slide'),
	(5,'video'),
	(6,'audio'),
	(7,'pdf'),
	(8,'compressed'),
	(9,'other'),
	(10,'youtube');

/*!40000 ALTER TABLE `ref_filetype` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_jenis_kelamin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_jenis_kelamin`;

CREATE TABLE `ref_jenis_kelamin` (
  `ref_jenis_kelamin_id` enum('L','P') NOT NULL,
  `jenis_kelamin` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ref_jenis_kelamin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ref_jenis_kelamin` WRITE;
/*!40000 ALTER TABLE `ref_jenis_kelamin` DISABLE KEYS */;

INSERT INTO `ref_jenis_kelamin` (`ref_jenis_kelamin_id`, `jenis_kelamin`)
VALUES
	('L','L'),
	('P','P');

/*!40000 ALTER TABLE `ref_jenis_kelamin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_kegiatan`;

CREATE TABLE `ref_kegiatan` (
  `ref_kegiatan_id` int(12) NOT NULL AUTO_INCREMENT,
  `kegiatan` varchar(128) DEFAULT NULL,
  `lokasi_kegiatan` varchar(45) DEFAULT NULL,
  `status_kegiatan` enum('HAJI','UMROH') NOT NULL DEFAULT 'UMROH',
  PRIMARY KEY (`ref_kegiatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `ref_kegiatan` WRITE;
/*!40000 ALTER TABLE `ref_kegiatan` DISABLE KEYS */;

INSERT INTO `ref_kegiatan` (`ref_kegiatan_id`, `kegiatan`, `lokasi_kegiatan`, `status_kegiatan`)
VALUES
	(1,'Wukuf','Padang Arafah','HAJI'),
	(2,'Wukuf','Padang Arafah','UMROH'),
	(3,'Thawaf','Ka\'bah','UMROH'),
	(4,'Bobok','Hotel','UMROH');

/*!40000 ALTER TABLE `ref_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_payment_method
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_payment_method`;

CREATE TABLE `ref_payment_method` (
  `ref_payment_method_id` int(12) NOT NULL AUTO_INCREMENT,
  `payment_method` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ref_payment_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ref_prasyarat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_prasyarat`;

CREATE TABLE `ref_prasyarat` (
  `ref_prasyarat_id` int(12) NOT NULL AUTO_INCREMENT,
  `prasyarat` varchar(45) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`ref_prasyarat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `ref_prasyarat` WRITE;
/*!40000 ALTER TABLE `ref_prasyarat` DISABLE KEYS */;

INSERT INTO `ref_prasyarat` (`ref_prasyarat_id`, `prasyarat`, `deskripsi`)
VALUES
	(1,'paspor','Paspor berlaku minimal 8 bulan dan nama di paspor harus 3 kata, contoh : Siti Aisyah Uni'),
	(2,'Pas foto terbaru','Berwarna\r\nLatar belakang (layar) foto berwarna putih Close up (Wajah terlihat 80%)\r\nTidak memakai kaca mata hitam\r\nWanita harus memakai jilbab  Ukuran foto: 4 x 6 = 6 lembar'),
	(3,'KTP','Foto Kopi KTP');

/*!40000 ALTER TABLE `ref_prasyarat` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_wilayah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_wilayah`;

CREATE TABLE `ref_wilayah` (
  `ref_wilayah_id` int(12) NOT NULL AUTO_INCREMENT,
  `kode_negara` varchar(5) NOT NULL,
  `kode_prov` varchar(5) NOT NULL,
  `kode_kab` varchar(5) NOT NULL,
  `kode_kec` varchar(5) NOT NULL,
  `kode_desa` varchar(5) NOT NULL,
  `status_kab` enum('KABUPATEN','KOTA') DEFAULT 'KABUPATEN',
  `status_desa` enum('DESA','KELURAHAN') DEFAULT 'DESA',
  PRIMARY KEY (`ref_wilayah_id`),
  UNIQUE KEY `index2` (`kode_desa`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table review_paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `review_paket`;

CREATE TABLE `review_paket` (
  `review_paket_id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(12) NOT NULL,
  `rate` enum('1','2','3','4','5') DEFAULT NULL,
  `review_comment` varchar(255) DEFAULT NULL,
  `travel_agent_id` int(12) DEFAULT NULL,
  `user_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`review_paket_id`),
  KEY `fk_review_paket_paket1_idx` (`paket_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `review_paket` WRITE;
/*!40000 ALTER TABLE `review_paket` DISABLE KEYS */;

INSERT INTO `review_paket` (`review_paket_id`, `paket_id`, `rate`, `review_comment`, `travel_agent_id`, `user_id`)
VALUES
	(1,1,'4','Ajiiiib',1,26),
	(2,1,'2','Maknyuuus',1,27),
	(3,1,'1','Mbeeeeek',1,28),
	(4,2,'4','ELeeeeee',1,29),
	(5,2,'5','Swasembada Pangaaaan',1,26),
	(6,2,'1','Looseeeers',1,27),
	(7,3,'4','Prikitiiiiw',2,28),
	(8,3,'3','Mbeeek',2,29),
	(9,4,'2','Tahu Sumedaaang',3,26),
	(10,4,'1','Teu aya nu ngelehan',3,27),
	(11,4,'4','kongkorongooook',3,28);

/*!40000 ALTER TABLE `review_paket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `role_id` int(12) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;

INSERT INTO `role` (`role_id`, `role`)
VALUES
	(1,'super admin'),
	(2,'admin travel'),
	(3,'koordinator travel'),
	(4,'Ustadz');

/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table setor_cicilan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `setor_cicilan`;

CREATE TABLE `setor_cicilan` (
  `setor_cicilan_id` int(12) NOT NULL AUTO_INCREMENT,
  `setor_total` int(11) DEFAULT NULL,
  `setor_ke` int(11) DEFAULT NULL,
  `jumlah_setoran` double DEFAULT NULL,
  `sisa_setoran` double DEFAULT NULL,
  `setor_tanggal` datetime DEFAULT NULL,
  `ref_payment_method_id` int(12) NOT NULL,
  `daftar_paket_umroh_id` int(12) NOT NULL,
  `user_bank_account_id` int(12) NOT NULL,
  `pemesanan_id` int(12) NOT NULL,
  `status_setor` enum('PENDING','CONFIRMED') DEFAULT 'PENDING',
  PRIMARY KEY (`setor_cicilan_id`),
  KEY `fk_setor_cicilan_ref_payment_method1_idx` (`ref_payment_method_id`) USING BTREE,
  KEY `fk_setor_cicilan_daftar_paket_umroh1_idx` (`daftar_paket_umroh_id`) USING BTREE,
  KEY `fk_setor_cicilan_user_bank_account1_idx` (`user_bank_account_id`) USING BTREE,
  CONSTRAINT `setor_cicilan_ibfk_1` FOREIGN KEY (`daftar_paket_umroh_id`) REFERENCES `anggota_paket` (`anggota_paket_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `setor_cicilan_ibfk_2` FOREIGN KEY (`ref_payment_method_id`) REFERENCES `ref_payment_method` (`ref_payment_method_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `setor_cicilan_ibfk_3` FOREIGN KEY (`user_bank_account_id`) REFERENCES `user_bank_account` (`user_bank_account_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table system_bank_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_bank_account`;

CREATE TABLE `system_bank_account` (
  `system_bank_account_id` int(12) NOT NULL AUTO_INCREMENT,
  `account_number` varchar(45) DEFAULT NULL,
  `account_branch` varchar(128) DEFAULT NULL,
  `account_name` varchar(45) DEFAULT NULL,
  `account_address` varchar(45) DEFAULT NULL,
  `ref_bank_id` int(12) NOT NULL,
  PRIMARY KEY (`system_bank_account_id`),
  KEY `fk_user_bank_account_ref_bank1_idx` (`ref_bank_id`) USING BTREE,
  CONSTRAINT `system_bank_account_ibfk_1` FOREIGN KEY (`ref_bank_id`) REFERENCES `ref_bank` (`ref_bank_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `system_bank_account` WRITE;
/*!40000 ALTER TABLE `system_bank_account` DISABLE KEYS */;

INSERT INTO `system_bank_account` (`system_bank_account_id`, `account_number`, `account_branch`, `account_name`, `account_address`, `ref_bank_id`)
VALUES
	(1,'9812346123','Kantor Cabang Ciputat 12345','Bang Haji','alamat 123',1),
	(2,'0846127324','Kantor Cabang Ciputat 12345','Dekurisme','alamat 123',2);

/*!40000 ALTER TABLE `system_bank_account` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tags_id` int(12) NOT NULL AUTO_INCREMENT,
  `tags` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tags_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table travel_agent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `travel_agent`;

CREATE TABLE `travel_agent` (
  `travel_agent_id` int(12) NOT NULL AUTO_INCREMENT,
  `travel_agent_code` varchar(45) DEFAULT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `alamat` text,
  `telp` varchar(45) DEFAULT NULL,
  `telp2` varchar(45) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `website` varchar(128) DEFAULT NULL,
  `penanggung_jawab` varchar(45) DEFAULT NULL,
  `kode_desa` varchar(5) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `hotline` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`travel_agent_id`),
  KEY `fk_travel_agent_ref_wilayah1_idx` (`kode_desa`) USING BTREE,
  CONSTRAINT `travel_agent_ibfk_1` FOREIGN KEY (`kode_desa`) REFERENCES `ref_wilayah` (`kode_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `travel_agent` WRITE;
/*!40000 ALTER TABLE `travel_agent` DISABLE KEYS */;

INSERT INTO `travel_agent` (`travel_agent_id`, `travel_agent_code`, `nama`, `alamat`, `telp`, `telp2`, `email`, `website`, `penanggung_jawab`, `kode_desa`, `image_path`, `hotline`, `description`)
VALUES
	(1,'AGENTCODE1','ANNISA TRAVEL','Jl. Raya Kutisari Indah No.135','(031) 8479331','08381232142','pesan@annisatravel.com','http://www.annisatravel.com','anisa',NULL,'annisa_travel.png','+8321','Annisa Travel adalah perusahaan swasta nasional yang bergerak dalam bidang Jasa Perjalanan dan didirikan di Jakarta pada tahun 2003.'),
	(2,'AGENTCODE2','AL-AMSOR','JL. WARUNG BUNCIT RAYA NO. 33-34, KALIBATA - PANCORAN\r\nKOTA JAKARTA SELATAN DKI JAKARTA','021 7944825','08381232142','alamsor_wisata@yahoo.com','http://al-amsor.com/web/','amsori',NULL,'alamsor.jpg','+231242','PT. AL AMSOR MUBAROKAH WISATA adalah perusahaan penyelenggara Umrah dan Haji Plus yang berpusat di kawasan Jakarta Selatan. Pengalaman kami dalam melayani jamaah Umrah dan Haji Plus sudah lebih dari sepuluh tahun. Legalitas kami pada Kemenag RI dengan nomor Umrah No. 244 Tahun 2015 dan Haji Plus No. D/ 641 Tahun 2012, bersertifikat Keanggotaan HIMPUH (Himpunan Penyelenggara Umrah dan Haji) dengan nomor 009/ HIMPUH/ 2010, ASITA (Asosiasi Perusahaan Perjalanan Indonesia) dengan NIA. 0787/ VIII/ DPP/ 2002, serta bersertifikat IATA (International Air Transport Association). Didukung tenaga-tenaga professional, kami siap memberikan pelayanan terbaik bagi calon tamu Allah swt. Kepercayaan jamaah merupakan semangat bagi kami, Insyaallah PT. AL AMSOR MUBAROKAH WISATA menjadi jalan terbaik untuk satu niat baik menuju Baitullah. Semoga Alloh SWT senantiasa meridhoi ikhtiar kita semua, amiin'),
	(3,'AGENTCODE3','PATUNA','RUKO KAV.BLOK A1 NO.6 JAKA SAMPURNA, KALIMALANG BEKASI SAMPING PERUM JAKA PERMAI',' (021)7228830','08381232142','patuna.bekasi@gmail.com','http://patunaumrohaji.com/home.php','patuna',NULL,'patuna_travel.jpg','+129382','PT PATUNA MEKAR JAYA atau lebih dikenal dengan nama PT PATUNA TOUR & TRAVEL adalah salah satu perusahaan yang bergerak dalam bidang jasa ticketing, tour, pengurusan dokumen perjalanan ( paspor dan visa ) serta Pelayanan Haji dan Umrah atau dikenal dengan PPIH ( Penyelenggara Perjalanan Ibadah Haji ) khusus.');

/*!40000 ALTER TABLE `travel_agent` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table umroh
# ------------------------------------------------------------

DROP TABLE IF EXISTS `umroh`;

CREATE TABLE `umroh` (
  `umroh_id` int(12) NOT NULL AUTO_INCREMENT,
  `tahun_masehi` varchar(4) DEFAULT NULL,
  `tahun_hijriah` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`umroh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `umroh` WRITE;
/*!40000 ALTER TABLE `umroh` DISABLE KEYS */;

INSERT INTO `umroh` (`umroh_id`, `tahun_masehi`, `tahun_hijriah`)
VALUES
	(1,'2015','1436');

/*!40000 ALTER TABLE `umroh` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(12) NOT NULL AUTO_INCREMENT,
  `NIK` varchar(45) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `alamat` text,
  `hash` varchar(128) DEFAULT NULL,
  `nama_lengkap` varchar(45) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `nomor_pasport` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `no_hp` varchar(16) DEFAULT NULL,
  `kode_desa` varchar(5) DEFAULT NULL,
  `status_verifikasi` enum('0','1') NOT NULL DEFAULT '0',
  `last_latitude` varchar(68) DEFAULT NULL,
  `last_longitude` varchar(68) DEFAULT NULL,
  `path_images` varchar(255) DEFAULT NULL,
  `umroh_flag` enum('TRUE','FALSE') NOT NULL DEFAULT 'FALSE',
  `gcm` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `umur` int(3) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `NIK_UNIQUE` (`NIK`) USING BTREE,
  UNIQUE KEY `EMAIL_UNIQUE` (`email`),
  KEY `fk_user_ref_jenis_kelamin_idx` (`jenis_kelamin`) USING BTREE,
  KEY `fk_user_ref_wilayah1_idx` (`kode_desa`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`jenis_kelamin`) REFERENCES `ref_jenis_kelamin` (`ref_jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`kode_desa`) REFERENCES `ref_wilayah` (`kode_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `NIK`, `password`, `alamat`, `hash`, `nama_lengkap`, `jenis_kelamin`, `nomor_pasport`, `email`, `no_hp`, `kode_desa`, `status_verifikasi`, `last_latitude`, `last_longitude`, `path_images`, `umroh_flag`, `gcm`, `created_at`, `updated_at`, `umur`)
VALUES
	(3,NULL,'$2y$10$aNx1I.nGexwJTXw0aQoQYO9s9zbiKSWL6YJvUXoabQXPkX.Yh0ZKq',NULL,NULL,'user1',NULL,NULL,'user1@email.com','083891465651',NULL,'0',NULL,NULL,'images.jpg','FALSE','cyX13oLXZxM:APA91bE3qZdjHOY5NV-XLhV51lLOUdYhPamz_SDim7SZgDZ0JUu-q06AgRvuNGrDlTDq5xDYtcMS8bJdiVzaYxANnfq36CPJb-RCNbl0Z2ndsO8nhfY5OgBSGvloceYMubexwP9IyHRA','2015-09-13 15:45:04','2016-02-27 05:48:14',NULL),
	(4,NULL,'$2y$10$aNx1I.nGexwJTXw0aQoQYO9s9zbiKSWL6YJvUXoabQXPkX.Yh0ZKq',NULL,NULL,'user2','L',NULL,'user2@email.com','083891465651',NULL,'0',NULL,NULL,'images.jpg','FALSE','cWD4OB0geuU:APA91bGccOoVAALVpMapkLtAz0eNOWDSyU3kX4faSuryCszXHFTSZr0Uxi1Bp0fyFSirPWq1jM2yRgbQyZGPyj7j2ExO55CgD-6pcpRJt9GCxqBSFIqzm-puC5Ns1FP529Tx8tpWHNhy','2015-09-13 15:45:04','2016-02-03 12:25:44',NULL),
	(5,NULL,'$2y$10$aNx1I.nGexwJTXw0aQoQYO9s9zbiKSWL6YJvUXoabQXPkX.Yh0ZKq',NULL,NULL,'user3','L',NULL,'user3@email.com','083891465651',NULL,'0',NULL,NULL,'images.jpg','FALSE','cUHc9BwXvwI:APA91bEp1OW6voyatVxgllgfPVnvi4ucIhnxizvSjpIeO8n4BWSdeVkkOKJHSAo6tXlxTs2YgU8ZqqJ6SWGPn3QbBro2q4nJMdPfHfjK7JCX1GRA-mK1sWZec4B6XTaQ_MX3K4HZsWHK','2015-09-13 15:45:04','2016-02-13 08:16:35',NULL),
	(6,NULL,'$2y$10$aNx1I.nGexwJTXw0aQoQYO9s9zbiKSWL6YJvUXoabQXPkX.Yh0ZKq',NULL,NULL,'user4','L',NULL,'user4@email.com','083891465651',NULL,'0',NULL,NULL,'images.jpg','FALSE','f6_am5_skqs:APA91bEqSyCQfoQj8IXBIohjdfD9cd5Tmy43KclOEV4rgwQ9-rDDiNOQPfnazCbVW4b2zTeBurdeXSGnhjDClcq5bNngXEOGlvQgCyEAxyMDWNAewyToOROLa7WEE-URuxk6_z2HeC9r','2015-09-13 15:45:04','2016-02-12 17:50:20',NULL),
	(7,NULL,'$2y$10$aNx1I.nGexwJTXw0aQoQYO9s9zbiKSWL6YJvUXoabQXPkX.Yh0ZKq',NULL,NULL,'user5','L',NULL,'user5@email.com','083891465651',NULL,'0',NULL,NULL,'images.jpg','FALSE','cWgxZkAe7Fo:APA91bFLcNx1JALipImXBOQUjuH1FMtaCKL_9UXkPiEDkzjPin3QVSVBYV_VPdZ-k92DIvrMM4RQd6K33MkUcgJJ5LcXRfi5VUTuY1AyiKE6-t0shRZA_dbIZCdgwud0P_8qQl1Tzb_D','2015-09-13 15:45:04','2016-02-12 15:26:48',NULL),
	(26,NULL,'$2y$10$kfecO6nxll6NEGI9iWHVq.C4oAs4sasHdIUVicAcB8P6xupL0bHVC',NULL,NULL,'Sidiq Permana',NULL,NULL,'permana.sidiq@gmail.com','082121212093',NULL,'0',NULL,NULL,'images.jpg','FALSE','testbcakcalcackba','2015-09-21 18:43:22','2015-12-28 05:50:02',NULL),
	(27,NULL,'$2y$10$hN5n9IiSlP7us3iUc21yseJwV/a8B2zoM5N.dQ.22hiQa/LaFSZxe',NULL,NULL,'Ghiyats Hanif Iskandar',NULL,NULL,'hanifghiyats@gmail.com','087873112903',NULL,'0',NULL,NULL,'images.jpg','FALSE','testbcakcalcackba','2015-09-23 18:01:08','2015-12-20 17:33:49',NULL),
	(28,NULL,'$2y$10$s2XiGQhzfXAABYs5OKKGAew7EkIxe3UrKJ1e4uMCrppKA/H6t1P/K',NULL,NULL,'taufan arfianto',NULL,NULL,'taufanarfianto@gmail.com','08561603287',NULL,'0',NULL,NULL,'images.jpg','FALSE','testbcakcalcackba','2015-09-23 20:57:26','2015-12-20 17:33:50',NULL),
	(29,NULL,'$2y$10$LEHGUyq748eo67m83MkYlu61f43zhBZKQy7AzB9vj57Zt3maitWHa',NULL,NULL,'Dede Kurniawan',NULL,NULL,'dekurisme@gmail.com','089666666673',NULL,'0',NULL,NULL,'images.jpg','FALSE','testbcakcalcackba','2015-09-24 14:49:23','2015-12-20 17:33:51',NULL),
	(30,NULL,'$2y$10$I1mMnMJUDfe5DOWlw0vQqOJS15BiCpE5Jm8y2nfXOfEDGeU3szY3i',NULL,NULL,'suhainda',NULL,NULL,'enda.cool@gmail.com','083878674610',NULL,'0',NULL,NULL,'images.jpg','FALSE','testbcakcalcackba','2015-12-03 14:11:31','2015-12-20 17:33:52',NULL),
	(33,NULL,'$2y$10$aNx1I.nGexwJTXw0aQoQYO9s9zbiKSWL6YJvUXoabQXPkX.Yh0ZKq','asdas',NULL,'yayaaaaaaan','L',NULL,'yayan@email.com','12312312',NULL,'0',NULL,NULL,'images.jpg','FALSE',NULL,'2016-01-28 16:01:03','2016-01-28 14:44:26',15),
	(34,NULL,'$2y$10$TWDv7kaQ.OoIMpky6JK4l.gYQCNdicdSrcWe3Wi6y1XsJxmaD2wrq','asdas',NULL,'yayaaaaaaan','L',NULL,'yayan2@email.com','12312312',NULL,'0',NULL,NULL,'images.jpg','FALSE',NULL,'2016-01-28 17:33:59','2016-01-28 14:44:26',15);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_bank_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_bank_account`;

CREATE TABLE `user_bank_account` (
  `user_bank_account_id` int(12) NOT NULL AUTO_INCREMENT,
  `account_number` varchar(45) DEFAULT NULL,
  `account_branch` varchar(128) DEFAULT NULL,
  `account_name` varchar(45) DEFAULT NULL,
  `account_address` varchar(45) DEFAULT NULL,
  `ref_bank_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`user_bank_account_id`),
  KEY `fk_user_bank_account_ref_bank1_idx` (`ref_bank_id`) USING BTREE,
  KEY `fk_user_bank_account_user1_idx` (`user_id`) USING BTREE,
  CONSTRAINT `user_bank_account_ibfk_1` FOREIGN KEY (`ref_bank_id`) REFERENCES `ref_bank` (`ref_bank_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_bank_account_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_document`;

CREATE TABLE `user_document` (
  `user_document_id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `ref_document_id` int(12) NOT NULL,
  `document_number` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_document_id`),
  KEY `fk_user_document_user1_idx` (`user_id`) USING BTREE,
  KEY `fk_user_document_ref_document1_idx` (`ref_document_id`) USING BTREE,
  CONSTRAINT `user_document_ibfk_1` FOREIGN KEY (`ref_document_id`) REFERENCES `ref_document` (`ref_document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_document_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_document_scan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_document_scan`;

CREATE TABLE `user_document_scan` (
  `user_document_scan_id` int(12) NOT NULL AUTO_INCREMENT,
  `user_document_id` int(12) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `ref_filetype_id` int(12) NOT NULL,
  PRIMARY KEY (`user_document_scan_id`),
  KEY `fk_user_document_scan_user_document1_idx` (`user_document_id`) USING BTREE,
  KEY `fk_user_document_scan_ref_filetype1_idx` (`ref_filetype_id`) USING BTREE,
  CONSTRAINT `user_document_scan_ibfk_1` FOREIGN KEY (`ref_filetype_id`) REFERENCES `ref_filetype` (`ref_filetype_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_document_scan_ibfk_2` FOREIGN KEY (`user_document_id`) REFERENCES `user_document` (`user_document_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ustadz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ustadz`;

CREATE TABLE `ustadz` (
  `ustadz_id` int(12) NOT NULL AUTO_INCREMENT,
  `admin_id` int(12) NOT NULL,
  PRIMARY KEY (`ustadz_id`),
  KEY `fk_ustadz_admin1_idx` (`admin_id`) USING BTREE,
  CONSTRAINT `ustadz_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ustadz_rating
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ustadz_rating`;

CREATE TABLE `ustadz_rating` (
  `ustadz_rating_id` int(12) NOT NULL AUTO_INCREMENT,
  `ustadz_id` int(12) NOT NULL,
  `rating` enum('1','2','3','4','5') DEFAULT NULL,
  `rating_at` timestamp NULL DEFAULT NULL,
  `rating_by` int(12) DEFAULT NULL,
  PRIMARY KEY (`ustadz_rating_id`),
  KEY `fk_ustadz_rating_ustadz1_idx` (`ustadz_id`) USING BTREE,
  KEY `fk_ustadz_rating_user1_idx` (`rating_by`) USING BTREE,
  CONSTRAINT `ustadz_rating_ibfk_1` FOREIGN KEY (`rating_by`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ustadz_rating_ibfk_2` FOREIGN KEY (`ustadz_id`) REFERENCES `ustadz` (`ustadz_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
