$(document).ready(function(){
	/*date picker*/
	$(function(){
		window.prettyPrint && prettyPrint();
		$('#dp1,#dp3').datepicker({
			format: 'mm-dd-yyyy'
		});
		$('#dp2,#dp4').datepicker();
		// disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#dpd1,#dpd3').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2,#dp4')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2,#dp4').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
	});
    

    /*tooltip*/
    $('.fasilitas,.ask,.list-fasilitas').tooltip({
      selector: "a[rel=tooltip]"
    })

    /*range price*/
    $(function() {
    $( "#slider" ).slider({
      range: true,
      min: 0,
      max: 5000,
      values: [ 1525, 3075 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider" ).slider( "values", 0 ) +
      " - $" + $( "#slider" ).slider( "values", 1 ) );
    });

    /*range rating*/
    $(function() {
    $( "#slider1" ).slider({
      range: true,
      min: 1,
      max: 5,
      values: [ 1, 5 ],
      slide: function( event, ui ) {
        $( "#amount1" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
      }
    });
    $( "#amount1" ).val(  $( "#slider1" ).slider( "values", 0 ) +
      " - " + $( "#slider1" ).slider( "values", 1 ) );
    });

    /*slider ustad*/

    $("#ust .price-mas-list").on("click", function() {
    $(this).siblings().addClass('dis').end().removeClass("dis");
  });
    /*$('.price-ust .carousel[data-type="multi"] .item').each(function(){
      var next = $(this).next();
      if (!next.length) {
        next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
      
      for (var i=0;i<2;i++) {
        next=next.next();
        if (!next.length) {
          next = $(this).siblings(':first');
        }
        
        next.children(':first-child').clone().appendTo($(this));
      }
    });*/
  /*same height*/

    equalheight = function(container){

    var currentTallest = 0,
         currentRowStart = 0,
         rowDivs = new Array(),
         $el,
         topPosition = 0;
     $(container).each(function() {

       $el = $(this);
       $($el).height('auto')
       topPostion = $el.position().top;

       if (currentRowStart != topPostion) {
         for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
           rowDivs[currentDiv].height(currentTallest);
         }
         rowDivs.length = 0; // empty the array
         currentRowStart = topPostion;
         currentTallest = $el.height();
         rowDivs.push($el);
       } else {
         rowDivs.push($el);
         currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
       for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
         rowDivs[currentDiv].height(currentTallest);
       }
     });
    }

    $(window).load(function() {
      equalheight('.travel-result .tra-res-wrap');
    });


    $(window).resize(function(){
      equalheight('.travel-result .tra-res-wrap');
    });

    
});
