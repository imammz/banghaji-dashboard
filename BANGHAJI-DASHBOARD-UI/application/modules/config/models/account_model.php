<?php
/**
 * Account_model Class
 *
 */
class account_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();   
        
	}
	
	// Inisialisasi nama tabel user
	var $table = 'user';
	
	/**
	 * Cek tabel user, apakah ada user dengan username dan password tertentu
	 */
	function check_user($email, $password)
	{
		$query = $this->db->get_where($this->table, array('email' => $email, 'password' => $password), 1, 0);
		// 
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}       
	
	public function _getUserByEmail($email) {
		$this->db->select('*');
		$this->db->from('user a');
		$this->db->where('email',$email);
		
		$ress = $this->db->get()->row_array();
		
		return $ress;
		
	}
	
}
// END Account_model Class

/* End of file account_model.php */ 
/* Location: ./system/application/model/login_model.php */