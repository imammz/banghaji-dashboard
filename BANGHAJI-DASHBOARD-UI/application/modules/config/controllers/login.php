<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends MX_Controller {

    function __construct() {
        parent::__construct();
         $this->load->library('encrypt_2015');
    }

    function index($wrong = false) {

        $this->checkLogout();
        
        if($wrong==TRUE) {
            echo "<script>alert('Email atau Password Anda Salah')</script>";
        }
        
        $data = array();
        $data['wrong'] = $wrong;
        $this->load->view('login_view', $data);
    }

    public function proccess() {

        $this->load->model("account_model");
        $account_model = new account_model();
        

        $this->output->enable_profiler(FALSE);

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if (!empty($email) || !empty($password)) {

            if ($account_model->check_user($email, $this->_encrypt_password_callback($password))) {
//            if ($this->account_model->check_user($email, $password)) {

                $rs = $account_model->_getUserByEmail($email);

                $data = array();
                $data = $rs;
                $_SESSION['login'] = TRUE;
                
                $_SESSION['user'] = $data;
                print_r($_SESSION); exit;
                redirect('home');
            } else {
                redirect('config/login/index/1');
            }
        } else {
            redirect('config/login/index');
        }
    }

    public function lockscreen() {
        $this->load->view('lockscreen_view');
    }

   
    
    private function _encrypt_password_callback($password) {
        $encrypt = new Encrypt_2015();
        $password = $encrypt->_base64_encrypt($password, $this->config->item('encryption_key'));
        return $password;
    }
    
     private function _decrypt_password_callback($password) {
        $encrypt = new Encrypt_2015();
        $password = $encrypt->_base64_decrypt($password, $this->config->item('encryption_key'));
        return $password;
    }

    function logout() {
        unset($_SESSION['login']);
        session_destroy();
        redirect("config/login/index");
    }

    function r3g1st3r($email, $password,$nama_lengkap) {

        $data = array();
        $data['email'] = $email;
        $data['password'] = $this->_encrypt_password_callback($password);
        $data['nama_lengkap'] = $nama_lengkap;

        if ($this->db->insert('user', $data)) {
            echo 'SUCCESS ENTRY NEW USER';
        } else {
            echo 'FAILED';
        }
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
