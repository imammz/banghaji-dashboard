<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
<div id="app">
    <?php echo Modules::run('templates/cliptwo/menu'); ?>
    <div class="app-content">
        <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
        <div class="main-content" >

            <section id="page-title" class="padding-top-15 padding-bottom-15">
                <div class="row">
                    <div class="col-sm-7">
                        <h1 class="mainTitle">Pemesanan</h1>
                    </div>
                </div>
            </section>

            <div class="wrap-content container container-fluid container-full bg-white" id="container">
                <!-- start: YOUR CONTENT HERE -->
                <div class="row"><div class="col-lg-12"><br/></div></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable">
                            <ul id="myTab1" class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_pemesanan" data-toggle="tab">
                                        Pemesanan Terakhir
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab_riwayat" data-toggle="tab">
                                        Riwayat Pemesanan
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab_pemesanan">
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-offset-3">
                                            <div class="row margin-bottom-15">
                                                <h2>Paket Promo V Maktour</h2>
                                                <h4>Tanggal 03/01/2016 s/d 03/01/2016</h4>
                                            </div>
                                            <div class="row margin-bottom-15">
                                                <h2 class="text-center text-green">Menunggu Konfirmasi Pembayaran</h2>
                                                <p class="text-justify no-margin">Lakukan Pembayaran melalui kartu kredit</p>
                                                <p>atau nomor Rekening bang haji xxx-********* atau Bank YYY-********</p>
                                                <p class="no-margin h3">Kemudian segera lakukan konfirmasi</p>
                                                <a href="#" class="text-blue">Klik untuk melakukan konfirmasi pembayaran</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_riwayat">
                                    <div class="row">
                                        <div class="col-lg-8 col-lg-offset-2">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Paket Pemesanan</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
</body>
</html>
