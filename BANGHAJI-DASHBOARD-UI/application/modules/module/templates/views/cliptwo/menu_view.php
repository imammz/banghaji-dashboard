<div class="sidebar app-aside" id="sidebar">
    <div class="sidebar-container perfect-scrollbar">
        <nav>
            <!-- start: SEARCH FORM -->
            <div class="search-form">
                <a class="s-open" href="#">
                    <i class="ti-search"></i>
                </a>
                <form class="navbar-form" role="search">
                    <a class="s-remove" href="#" target=".navbar-form">
                        <i class="ti-close"></i>
                    </a>
                    
                </form>
            </div>
            <!-- end: SEARCH FORM -->
            <!-- start: MAIN NAVIGATION MENU -->
            <div class="navbar-title">
                <span>Bang Haji User Dashboard</span>
            </div>
            <ul class="main-navigation-menu">
                <li class="<?php echo ($page['module']=='beranda'&&$page['class']=='beranda')?'active':''; ?>">
                    <a href="<?php echo site_url('beranda/beranda')?>">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Beranda </span><i class="icon-arrow"></i>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="<?php echo ($page['module']=='profil'&&$page['class']=='profil')?'active':''; ?>">
                    <a href="<?php echo site_url('profil/profil')?>">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Profil </span><i class="icon-arrow"></i>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="<?php echo ($page['module']=='pemesanan'&&$page['class']=='pemesanan')?'active':''; ?>">
                    <a href="<?php echo site_url('pemesanan/pemesanan')?>">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="fa fa-external-link-square"></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Pemesanan </span><i class="icon-arrow"></i>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="<?php echo ($page['module']=='perjalanan'&&$page['class']=='perjalanan')?'active':''; ?>">
                    <a href="<?php echo site_url('perjalanan/perjalanan')?>">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="fa fa-file-archive-o"></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Perjalanan Saya </span><i class="icon-arrow"></i>
                            </div>
                        </div>
                    </a>
                </li>
              
                
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </nav>
    </div>
</div>