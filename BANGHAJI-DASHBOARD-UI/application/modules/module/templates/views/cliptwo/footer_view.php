<!-- start: FOOTER -->
<footer>
    <div class="footer-inner">
        <div class="pull-left">
            &copy; <span class="current-year"></span> <span class="text-bold text-uppercase"><?php echo WEB_TITLE ?></span>. 
        </div>
    </div>
</footer>
<!-- end: FOOTER -->