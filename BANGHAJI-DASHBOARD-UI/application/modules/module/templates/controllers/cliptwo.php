<?php

class Cliptwo extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
   
    public function header()
    {
        $this->load->view('cliptwo/header_view');
    }
	public function menu()
    {
        $this->load->view('cliptwo/menu_view');
    }

	public function topnavbar()
    {
        $this->load->view('cliptwo/topnavbar_view');
    }
    
    public function footer()
    {
        $this->load->view('cliptwo/footer_view');
    }
    
    public function js() {
        $this->load->view('cliptwo/js_view');
    }

	
}

?>