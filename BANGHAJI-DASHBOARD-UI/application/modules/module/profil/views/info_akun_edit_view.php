<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <section id="page-title" class="padding-top-15 padding-bottom-15">
                    <div class="row">
                        <div class="col-sm-7">
                            <h1 class="mainTitle">Profil</h1>
                        </div>
                    </div>
                </section>

                <div class="wrap-content container container-fluid container-full bg-white" id="container">
                    <!-- start: YOUR CONTENT HERE -->
                    <div class="row"><div class="col-lg-12"><br/></div></div>

                    <div class="row">
                        <div class="col-md-12"> 

                            <div class="tabbable">
                                <ul id="myTab1" class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_akun" data-toggle="tab">
                                            Informasi Akun
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_biodata" data-toggle="tab">
                                            Biodata
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_info" data-toggle="tab">
                                            Info Pembayaran
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab_akun">

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <fieldset>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="user-image">
                                                            <div class="fileinput-new thumbnail"><img src="<?php echo base_url() ?>assets/images/avatar-1-xl.jpg" alt="">
                                                            </div>

                                                            <div class="user-image-buttons">
                                                                <span class="btn btn-azure btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-pencil"></i></span><span class="fileinput-exists"><i class="fa fa-pencil"></i></span>
                                                                    <input type="file" onchange="alert('Upload Berhasil')">
                                                                </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <div class="center">
                                                        <h3>Bang Haji Code Aktif Anda Saat Ini</h3>
                                                        <h2> <span class="label label-sm label-success">781BA</span></h2>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-sm-8">
                                                <form action="" method="post">
                                                    <fieldset>
                                                        <table class="table table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th colspan="1">Dian Sastro Wardoyo</th>
                                                                <th><a href="<?php echo site_url('/') ?>profil"><i class="fa fa-pencil edit-user-info"></i> Save Data</a></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <tr>
                                                                <td>Email</td>
                                                                <td>
                                                                    <input type="text" id="" class="form-control" value="peter@example.com">
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td>Mobile Phone</td>
                                                                <td><input type="text" id="" class="form-control" value="(641)-734-476"></td>

                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>
                                                                    <a href="">
                                                                        <span class="label label-sm label-warning">Belum Verifikasi</span>
                                                                    </a></td>

                                                            </tr>
                                                            <tr>
                                                                <td>Password</td>
                                                                <td>
                                                                    <a href="" class="text-danger">
                                                                        Ubah Password
                                                                    </a></td>

                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </fieldset>
                                                </form>
                                                <fieldset>
                                                    <div class="panel-heading border-light">
                                                        <h4 class="panel-title text-primary">Logs Aktifitas</h4>
                                                        <paneltool class="panel-tools" tool-collapse="tool-collapse" tool-refresh="load1" tool-dismiss="tool-dismiss"></paneltool>
                                                    </div>
                                                    <div collapse="activities"  class="panel-wrapper">
                                                        <div class="panel-body">
                                                            <ul class="timeline-xs">
                                                                <li class="timeline-item success">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            2 minutes ago
                                                                        </div>
                                                                        <p>
                                                                            <a class="text-info" href>
                                                                                Steven
                                                                            </a>
                                                                            has completed his account.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            12:30
                                                                        </div>
                                                                        <p>
                                                                            Staff Meeting
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item danger">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            11:11
                                                                        </div>
                                                                        <p>
                                                                            Completed new layout.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item info">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Thu, 12 Jun
                                                                        </div>
                                                                        <p>
                                                                            Contacted
                                                                            <a class="text-info" href>
                                                                                Microsoft
                                                                            </a>
                                                                            for license upgrades.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Tue, 10 Jun
                                                                        </div>
                                                                        <p>
                                                                            Started development new site
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Sun, 11 Apr
                                                                        </div>
                                                                        <p>
                                                                            Lunch with
                                                                            <a class="text-info" href>
                                                                                Nicole
                                                                            </a>
                                                                            .
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item warning">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Wed, 25 Mar
                                                                        </div>
                                                                        <p>
                                                                            server Maintenance.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Fri, 20 Mar
                                                                        </div>
                                                                        <p>
                                                                            New User Registration
                                                                            <a class="text-info" href>
                                                                                more details
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="tab_biodata">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <form action="" method="post">
                                                    <fieldset>
                                                        <legend>Data Pribadi</legend>
                                                        <table class="table table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th colspan="1"></th>
                                                                <th><a href="<?php echo site_url('/') ?>profil" class="show-tab"><i class="fa fa-pencil edit-user-info"></i> Save Data</a></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>Nama Lengkap</td>
                                                                <td><input type="text" class="form-control" value="Dian Sastro Wardoyo"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>NIK</td>
                                                                <td><input type="text" class="form-control" value="220000044433221"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mobile Phone</td>
                                                                <td><input type="text" class="form-control" value="(641)-734-4763"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>
                                                                    <a href="">
                                                                        <span class="label label-sm label-warning">Belum Verifikasi</span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Password</td>
                                                                <td>
                                                                    <a href="" class="text-danger">
                                                                        Ubah Password
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Alamat</td>
                                                                <td><textarea name="" id="" cols="30" rows="10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi bibendum orci orci, tempus mattis massa imperdiet sit amet. Duis tristique dui dictum risus euismod volutpat. Vestibulum justo dolor, placera</textarea>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Provinsi</td>
                                                                <td>
                                                                    <select name="" id="" class="form-control">
                                                                        <option value="A">A</option>
                                                                        <option value="A">A</option>
                                                                        <option value="A">A</option>
                                                                        <option value="A">A</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Kabupaten / Kota </td>
                                                                <td>
                                                                    <select name="" id="" class="form-control">
                                                                        <option value=""></option>
                                                                        <option value="A">A</option>
                                                                        <option value="A">A</option>
                                                                        <option value="A">A</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jenis Kelamin</td>
                                                                <td>
                                                                    <select name="" id="" class="form-control">
                                                                        <option value=""></option>
                                                                        <option value="L">L</option>
                                                                        <option value="P">P</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </fieldset>

                                                </form>
                                            </div>
                                            <div class="col-lg-6">
                                                <fieldset>
                                                    <legend>Dokumen</legend>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_info">
                                        <fieldset>
                                            <legend>
                                                Transfer Bank
                                            </legend>
                                            <div class="col-lg-6">
                                                <div class="row margin-bottom-10">
                                                    <label for="bank" class="control-label col-lg-4">Bank :</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="bank" >
                                                    </div>
                                                </div>
                                                <div class="row margin-bottom-10">
                                                    <label for="kantor_cabang" class="control-label col-lg-4">Kantor Cabang :</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="kantor_cabang">
                                                    </div>
                                                </div>
                                                <div class="row margin-bottom-10">
                                                    <label for="atas_nama" class="control-label col-lg-4">Atas Nama :</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="atas_nama">
                                                    </div>
                                                </div>
                                                <div class="row margin-bottom-10">
                                                    <label for="no_rek" class="control-label col-lg-4">No Rekening :</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="no_rek">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <a href="<?php echo site_url('/') ?>profil" class="btn btn-o btn-primary">
                                                            Save
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end: YOUR CONTENT HERE -->

                </div>
            </div>
            <?php echo Modules::run('templates/cliptwo/footer'); ?>
        </div>
        <?php echo Modules::run('templates/cliptwo/js'); ?>
        <!-- start: JavaScript Event Handlers for this page -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
</body>
</html>
