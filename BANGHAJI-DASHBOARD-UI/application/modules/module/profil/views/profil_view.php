<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <section id="page-title" class="padding-top-15 padding-bottom-15">
                    <div class="row">
                        <div class="col-sm-7">
                            <h1 class="mainTitle">Profil</h1>
                        </div>
                    </div>
                </section>

                <div class="wrap-content container container-fluid container-full bg-white" id="container">
                    <!-- start: YOUR CONTENT HERE -->
                    <div class="row"><div class="col-lg-12"><br/></div></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable">
                                <ul id="myTab1" class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_akun" data-toggle="tab">
                                            Informasi Akun
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_biodata" data-toggle="tab">
                                            Biodata
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_info" data-toggle="tab">
                                            Info Pembayaran
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab_akun">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <fieldset>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="user-image">
                                                            <div class="fileinput-new thumbnail"><img src="<?php echo base_url() ?>assets/images/avatar-1-xl.jpg" alt="">
                                                            </div>

                                                            <div class="user-image-buttons">
                                                                <span class="btn btn-azure btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-pencil"></i></span><span class="fileinput-exists"><i class="fa fa-pencil"></i></span>
                                                                    <input type="file" onchange="alert('Upload Berhasil')">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <div class="center">
                                                        <h3>Bang Haji Code Aktif Anda Saat Ini</h3>
                                                        <h2> <span class="label label-sm label-success">781BA</span></h2>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-sm-8">
                                                <fieldset>
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="1">Dian Sastro Wardoyo</th>
                                                                <th><a href="<?php echo site_url('/') ?>profil/info_akun_edit"><i class="fa fa-pencil edit-user-info"></i> Edit Data</a></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Email</td>
                                                                <td>
                                                                    peter@example.com
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mobile Phone</td>
                                                                <td>(641)-734-4763</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>
                                                                    <a href="">
                                                                        <span class="label label-sm label-warning">Belum Verifikasi</span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Password</td>
                                                                <td>
                                                                    <a href="" class="text-danger">
                                                                        Ubah Password
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </fieldset>
                                                <fieldset>
                                                    <div class="panel-heading border-light">
                                                        <h4 class="panel-title text-primary">Logs Aktifitas</h4>
                                                        <paneltool class="panel-tools" tool-collapse="tool-collapse" tool-refresh="load1" tool-dismiss="tool-dismiss"></paneltool>
                                                    </div>
                                                    <div collapse="activities"  class="panel-wrapper">
                                                        <div class="panel-body">
                                                            <ul class="timeline-xs">
                                                                <li class="timeline-item success">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            2 minutes ago
                                                                        </div>
                                                                        <p>
                                                                            <a class="text-info" href>
                                                                                Steven
                                                                            </a>
                                                                            has completed his account.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            12:30
                                                                        </div>
                                                                        <p>
                                                                            Staff Meeting
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item danger">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            11:11
                                                                        </div>
                                                                        <p>
                                                                            Completed new layout.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item info">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Thu, 12 Jun
                                                                        </div>
                                                                        <p>
                                                                            Contacted
                                                                            <a class="text-info" href>
                                                                                Microsoft
                                                                            </a>
                                                                            for license upgrades.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Tue, 10 Jun
                                                                        </div>
                                                                        <p>
                                                                            Started development new site
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Sun, 11 Apr
                                                                        </div>
                                                                        <p>
                                                                            Lunch with
                                                                            <a class="text-info" href>
                                                                                Nicole
                                                                            </a>
                                                                            .
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item warning">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Wed, 25 Mar
                                                                        </div>
                                                                        <p>
                                                                            server Maintenance.
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li class="timeline-item">
                                                                    <div class="margin-left-15">
                                                                        <div class="text-muted text-small">
                                                                            Fri, 20 Mar
                                                                        </div>
                                                                        <p>
                                                                            New User Registration
                                                                            <a class="text-info" href>
                                                                                more details
                                                                            </a>
                                                                            .
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="tab_biodata">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <fieldset>
                                                    <legend>Data Pribadi</legend>
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="1"></th>
                                                                <th><a href="<?php echo site_url('/') ?>profil/info_akun_edit"><i class="fa fa-pencil edit-user-info"></i> Edit Data</a></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Nama Lengkap</td>
                                                                <td>Dian Sastro Wardoyo</td>
                                                            </tr>
                                                            <tr>
                                                                <td>NIK</td>
                                                                <td>220000044433221</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mobile Phone</td>
                                                                <td>(641)-734-4763</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>
                                                                    <a href="">
                                                                        <span class="label label-sm label-warning">Belum Verifikasi</span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Password</td>
                                                                <td>
                                                                    <a href="" class="text-danger">
                                                                        Ubah Password
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Alamat</td>
                                                                <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi bibendum orci orci, tempus mattis massa imperdiet sit amet. Duis tristique dui dictum risus euismod volutpat. Vestibulum justo dolor, placera</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Provinsi</td>
                                                                <td>Banten</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Kabupaten / Kota </td>
                                                                <td>Tangerang</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jenis Kelamin</td>
                                                                <td>P</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </fieldset>
                                            </div>
                                            <div class="col-lg-6">
                                                <fieldset>
                                                    <legend>Dokumen</legend>
                                                </fieldset>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_info">
                                        <fieldset>
                                            <legend>
                                                Transfer Bank
                                            </legend>
                                            <div class="col-lg-6">
                                                <div class="row margin-bottom-10">
                                                    Bank :
                                                </div>
                                                <div class="row margin-bottom-10">
                                                    Kantor Cabang :
                                                </div>
                                                <div class="row margin-bottom-10">
                                                    Atas Nama :
                                                </div>
                                                <div class="row margin-bottom-10">
                                                    Nomor Rekening :
                                                </div>
                                                <div class="row">
                                                    <a href="<?php echo site_url('/') ?>profil/info_akun_edit" class="btn btn-o btn-primary">
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <form action="" method="post">
                                            <fieldset>
                                                <legend>
                                                    Kartu Kredit
                                                </legend>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>
                                                            Please select payment method
                                                        </label>
                                                        <div class="form-group">
                                                            <div class="btn-group" data-toggle="buttons">
                                                                <label class="btn btn-primary active">
                                                                    <input type="radio" name="paymentMethod" id="option1" autocomplete="off" value="Visa">
                                                                    <i class="fa fa-cc-visa" ></i> Visa
                                                                </label>
                                                                <label class="btn btn-primary active">
                                                                    <input type="radio" name="paymentMethod" id="option1" autocomplete="off" value="Master Card">
                                                                    <i class="fa fa-cc-mastercard" ></i> Master Card
                                                                </label>
                                                                <label class="btn btn-primary active">
                                                                    <input type="radio" name="paymentMethod" id="option1" autocomplete="off" value="Amex">
                                                                    <i class="fa fa-cc-amex" ></i> Amex
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>
                                                                Card Number
                                                            </label>
                                                            <input type="text" class="form-control" name="cardNumber" placeholder="Enter Your Card Number">
                                                        </div>
                                                        <label>
                                                            Expires
                                                        </label>
                                                        <div class="row">
                                                            <div class="col-md-4 col-sm-6">
                                                                <div class="form-group">
                                                                    <select class="cs-select cs-skin-slide">
                                                                        <option value="" disabled>Month</option>
                                                                        <option value="January">January</option>
                                                                        <option value="February">February</option>
                                                                        <option value="March">March</option>
                                                                        <option value="April">April</option>
                                                                        <option value="May">May</option>
                                                                        <option value="June">June</option>
                                                                        <option value="July">July</option>
                                                                        <option value="August">August</option>
                                                                        <option value="September">September</option>
                                                                        <option value="October">October</option>
                                                                        <option value="November">November</option>
                                                                        <option value="December">December</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-6">
                                                                <div class="form-group">
                                                                    <select class="cs-select cs-skin-slide">
                                                                        <option value="" disabled>Year</option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2020">2020</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <label>
                                                                    Security code
                                                                </label>
                                                                <div class="row">
                                                                    <div class="col-xs-3">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control" name="securityCode" placeholder="Security code">
                                                                        </div>
                                                                    </div>
                                                                    <span class="help-inline col-xs-4">
                                                                        <a href="javascript:void(0)" data-content="The security code is a three-digit number on the back of your credit card, immediately following your main card number." data-title="How to find the security code" data-placement="top" data-toggle="popover">
                                                                            <i class="ti-help-alt text-large text-primary"></i>
                                                                        </a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <label>
                                                            Billing Address
                                                        </label>
                                                        <div class="row margin-bottom-5">
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" placeholder="First Name">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" placeholder="Last Name">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-bottom-5">
                                                            <div class="col-lg-12">
                                                                <input type="text" class="form-control" placeholder="Billing Address">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-bottom-5">
                                                            <div class="col-lg-12">
                                                                <input type="text" class="form-control" placeholder="State">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-bottom-5">
                                                            <div class="col-lg-4">
                                                                <input type="text" class="form-control" placeholder="City">
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <input type="text" class="form-control" placeholder="Zip Code">
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <input type="text" class="form-control" disabled value="indonesia">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <a href="<?php echo site_url('/') ?>profil/info_akun_edit" class="btn btn-o btn-primary">
                                                            Submit Credit Card
                                                        </a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end: YOUR CONTENT HERE -->

                </div>
            </div>
            <?php echo Modules::run('templates/cliptwo/footer'); ?>
        </div>
        <?php echo Modules::run('templates/cliptwo/js'); ?>
        <!-- start: JavaScript Event Handlers for this page -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
</body>
</html>
