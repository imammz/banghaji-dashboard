<?php

class Profil extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
        //$this->checkLogin();
    }
    
    	public function index()
	{ 
            $this->output->enable_profiler(false);
            
            $pages = array(
                'module' => 'profil',
                'class' => 'profil',
                'function' => 'index'
            );
            
            $data = array();
            $data['page'] = $pages;
            $data['user'] = (isset($_SESSION['user']))?$_SESSION['user']:'';
            
            $this->load->view('profil_view',$data);
	}
        
        public function info_akun_edit()
	{ 
            $this->output->enable_profiler(false);
            
            $pages = array(
                'module' => 'profil',
                'class' => 'profil',
                'function' => 'index'
            );
            
            $data = array();
            $data['page'] = $pages;
            $data['user'] = (isset($_SESSION['user']))?$_SESSION['user']:'';
            
            $this->load->view('info_akun_edit_view',$data);
	}
        
        public function info_akun_proses()
	{ 
          
            redirect('profil/profil/');
	}

}

?>