<?php

class Beranda extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
        //$this->checkLogin();
    }
    
    	public function index()
	{ 
            $this->output->enable_profiler(false);
            
            $pages = array(
                'module' => 'beranda',
                'class' => 'beranda',
                'function' => 'index'
            );
            
            $data = array();
            $data['page'] = $pages;
            $data['user'] = (isset($_SESSION['user']))?$_SESSION['user']:'';
            
            $this->load->view('beranda_view',$data);
	}
        
        
        
        

}

?>