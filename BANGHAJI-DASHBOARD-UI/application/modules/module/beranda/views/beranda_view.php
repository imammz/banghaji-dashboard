<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <section id="page-title">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="mainTitle">
                                <i>Bang Haji Code Aktif Anda Saat Ini </i> : 
                                781BA
                            </h1>
                        </div>
                    </div>
                </section>

                <div class="wrap-content container container-fluid container-full bg-white" id="container">
                    <!-- start: YOUR CONTENT HERE -->
                    <div class="row"><div class="col-lg-12"><br/></div></div>
                    
                            <div class="row"><div class="col-lg-12">
                                    <p>
                                        Halaman Dashboard Bang Haji. Disini anda bisa melakukan pergantian Profil berupa data akun, biodata, ganti password juga bisa melakukan tindak lanjut dari pemesanan anda yang belum selesai.
 
Dashboard ini juga dilengkapi halaman untuk melihat riwayat perjalanan umroh anda selama menggunakan applikasi Bang Haji. 
                                    </p>
                                    
                        </div></div>
                    
                    
                    <div class="row">
                        <div class="col-md-12"> 
                            
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="panel panel-white no-radius text-center">
                                        <div class="panel-body" style="min-height: 250px">
                                           <p class="links cl-effect-1">
                                                <a href="<?php echo site_url('pemesananan/pemesanan/getLast/'.(isset($user['user_id'])?$user['user_id']:'') )  ?> ">
                                            <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa fa-share fa-stack-1x fa-inverse"></i> </span>
                                            <h2 class="StepTitle">Pemesananan Terakhir</h2>
                                                </a>
                                               Informasi Pemesanan Paket Umroh Terakhir Anda
                                           </p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                    <div class="panel panel-white no-radius text-center">
                                        <div class="panel-body" style="min-height: 250px">
                                           <p class="links cl-effect-1">
                                                <a href="<?php echo site_url('pemesananan/pemesanan/getLast/'.(isset($user['user_id'])?$user['user_id']:'') )  ?> ">
                                            <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa fa-smile-o fa-stack-1x fa-inverse"></i> </span>
                                            <h2 class="StepTitle">Perjalanan Umroh</h2>
                                                </a>
                                               Paket Umroh Ceria Tanggal <?php echo date('d-m-Y')?>
                                           </p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                    <div class="panel panel-white no-radius text-center">
                                        <div class="panel-body" style="min-height: 250px">
                                           <p class="links cl-effect-1">
                                                <a href="<?php echo site_url('pemesananan/pemesanan/getLast/'.(isset($user['user_id'])?$user['user_id']:'') )  ?> ">
                                            <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa fa-smile-o fa-stack-1x fa-inverse"></i> </span>
                                            <h2 class="StepTitle">Upload Foto</h2>
                                                </a>
                                               Upload Foto Pengalaman Umroh Anda Pada Paket Umroh Cerita Tanggal <?php echo date('d-m-Y')?>
                                           </p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- end: YOUR CONTENT HERE -->

                </div>
            </div>
            <?php echo Modules::run('templates/cliptwo/footer'); ?>
        </div>
        <?php echo Modules::run('templates/cliptwo/js'); ?>
        <!-- start: JavaScript Event Handlers for this page -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
</body>
</html>
