<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
<div id="app">
    <?php echo Modules::run('templates/cliptwo/menu'); ?>
    <div class="app-content">
        <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
        <div class="main-content" >

            <section id="page-title" class="padding-top-15 padding-bottom-15">
                <div class="row">
                    <div class="col-sm-7">
                        <h1 class="mainTitle">Riwayat Perjalanan Haji/Umroh Anda</h1>
                    </div>
                </div>
            </section>

            <div class="wrap-content container container-fluid container-full bg-white" id="container">
                <!-- start: YOUR CONTENT HERE -->
                <div class="row"><div class="col-lg-12"><br/></div></div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <ul class="pagination margin-bottom-5">
                            <li>
                                <a href="#">
                                    <i class="ti-arrow-left"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    1
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    2
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    3
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    4
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-arrow-right"></i>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
</body>
</html>
