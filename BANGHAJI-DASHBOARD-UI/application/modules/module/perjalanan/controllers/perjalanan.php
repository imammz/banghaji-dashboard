<?php

class Perjalanan extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
//        $this->checkLogin();
    }

    public function index()
    {
        $this->output->enable_profiler(false);

        $pages = array(
            'module' => 'perjalanan',
            'class' => 'perjalanan',
            'function' => 'index'
        );

        $data = array();
        $data['page'] = $pages;
        $data['user'] = (isset($_SESSION['user']))?$_SESSION['user']:'';

        $this->load->view('home_view',$data);
    }


    public function detail()
    {
        $this->output->enable_profiler(false);

        $pages = array(
            'module' => 'perjalanan',
            'class' => 'perjalanan',
            'function' => 'index'
        );

        $data = array();
        $data['page'] = $pages;
        $data['user'] = (isset($_SESSION['user']))?$_SESSION['user']:'';

        $this->load->view('detail_view',$data);
    }





}

?>