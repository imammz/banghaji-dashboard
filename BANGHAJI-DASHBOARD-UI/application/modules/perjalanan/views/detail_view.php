<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
<div id="app">
    <?php echo Modules::run('templates/cliptwo/menu'); ?>
    <div class="app-content">
        <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
        <div class="main-content" >

            <section id="page-title" class="padding-top-15 padding-bottom-15">
                <div class="row">
                    <div class="col-sm-7">
                        <h1 class="mainTitle">Pemesanan</h1>
                    </div>
                </div>
            </section>

            <div class="wrap-content container container-fluid container-full bg-white" id="container">
                <!-- start: YOUR CONTENT HERE -->
                <div class="row"><div class="col-lg-12"><br/></div></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row margin-bottom-5">
                            <div class="col-lg-2">
                                <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                            </div>
                            <div class="col-lg-8">
                                <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                <h3 class="bold">Bang Haji Code : C187AF</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tabbable">
                                    <ul id="myTab1" class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_gallery" data-toggle="tab">
                                                Gallery
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_jadwal" data-toggle="tab">
                                                Jadwal Kegiatan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_perjalanan" data-toggle="tab">
                                                Info Perjalanan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_pemesanan" data-toggle="tab">
                                                Info Pemesanan
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab_gallery">
                                            <div class="row margin-bottom-15">
                                                <div class="col-lg-12">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive col-lg-3" width="150" height="150">
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row margin-bottom-5">
                                                <h2 class="col-lg-12">Perjalanan Umroh Lainnya</h2>
                                                <div class="col-lg-2">
                                                    <img src="<?php echo base_url()?>assets/images/Home---Umroh_11.png" alt="" class="img img-rounded img-responsive" width="150" height="150">
                                                </div>
                                                <div class="col-lg-8">
                                                    <h2 class="bold ">Paket Umroh Ceria Maktour 3 Jan 2016</h2>
                                                    <p class="description">Perjalanan Umroh Plus disertai dengan wisata Rohani ke Mesir </p>
                                                    <a href="<?php echo site_url() ?>perjalanan/detail" class="pull-right h3">Selengkapnya</a>
                                                    <h3 class="bold">Bang Haji Code : C187AF</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab_jadwal">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="full-calendar"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab_perjalanan">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h3>Berangkat dari Jakarta</h3>
                                                    <h3>Maskapai berangkat</h3>
                                                    <h3>Hotel di Jeddah</h3>
                                                    <h3>Perjalanan ke Madinah</h3>
                                                    <h3>Hotel di Madinah</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab_pemesanan">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h3>Tanggal Pemesanan</h3>
                                                    <h3>Tanggal Konfirmasi</h3>
                                                    <h3>Total Biaya Paket Umroh</h3>
                                                    <h3>Pembayaran melalui</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->
            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>

    <script src="<?php echo base_url()?>vendor/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="<?php echo base_url()?>vendor/moment/moment.min.js"></script>
    <script src="<?php echo base_url()?>vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url()?>vendor/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url()?>vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <!-- start: JavaScript Event Handlers for this page -->
    <script src="<?php echo base_url()?>assets/js/pages-calendar.js"></script>
    <script>
        jQuery(document).ready(function () {
            Main.init();
            Calendar.init();
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
</body>
</html>
