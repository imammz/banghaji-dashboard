<!-- start: MAIN JAVASCRIPTS -->
		<script src="<?php echo base_url()?>vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url()?>vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>vendor/modernizr/modernizr.js"></script>
		<script src="<?php echo base_url()?>vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url()?>vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="<?php echo base_url()?>vendor/switchery/switchery.min.js"></script>
		
                <script src="<?php echo base_url()?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
		<script src="<?php echo base_url()?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
		<script src="<?php echo base_url()?>vendor/autosize/autosize.min.js"></script>
		<script src="<?php echo base_url()?>vendor/selectFx/classie.js"></script>
		<script src="<?php echo base_url()?>vendor/selectFx/selectFx.js"></script>
                <script src="<?php echo base_url()?>vendor/select2/select2.min.js"></script>
                <script src="<?php echo base_url()?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<script src="<?php echo base_url()?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: CLIP-TWO JAVASCRIPTS -->
		<script src="<?php echo base_url()?>assets/js/main.js"></script>
		<script src="<?php echo base_url()?>assets/js/form-elements.js"></script>