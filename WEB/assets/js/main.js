$(document).ready(function(){

	/*big carousel*/
	$('#bigCarousel,#testSlide').carousel();

	/*paket slide*/
	$('#paketSlide,#partnerSlide, #ustadSlide').carousel({
		interval: false
	});
	

	/*tab promo &sponsor*/
	$('#Tabs1 a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})

	/*date picker*/
	$(function(){
		window.prettyPrint && prettyPrint();
		$('#dp1').datepicker({
			format: 'mm-dd-yyyy'
		});
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '/' +
        (month<10 ? '0' : '') + month + '/' +
        (day<10 ? '0' : '') + day;
        $("#dpd1,#dpd2").val(output);



		$('#dp2').datepicker();
		// disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
	});

	/*+- Qty*/
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('#qty').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#qty').val(0);
        }
    });
     $('.qtyplus1').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty1').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('#qty1').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#qty1').val(0);
        }
    });
     // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('#qty').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('#qty').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus1").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty1').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('#qty1').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('#qty1').val(0);
        }
    });

	
	/*tooltip*/
	$('.fasilitas,.ask,.list-fasilitas').tooltip({
	  selector: "a[rel=tooltip]"
	})


    /*calendar*/

    $('.calendar').fullCalendar({
            header: {
                left: '',
                center: '',
                right: ''
            },
            lang: 'id',
            defaultDate: '2015-11-01',
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: '$3500',
                    start: '2015-11-24'
                },
                
                {
                     title: '$2500',
                    start: '2015-11-25'
                },
                {
                     title: '$2000',
                    start: '2015-11-26'
                },
                {
                     title: '$3500',
                    start: '2015-11-30'
                },
                {
                    title: '$1500',
                    start: '2015-11-31'
                }
            ]
        });
});
