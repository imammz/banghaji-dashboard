searchVisible = 0;
transparent = true;

$(document).ready(function(){
    /*phone input*/
    $("#phone").intlTelInput({
        utilsScript: "js/utils.js"
      });
    /*wizard form*/ 
    $('#wizard').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        /*'nextSelector': '.button-next', 'previousSelector': '.button-previous',*/
         onInit : function(tab, navigation,index){
         
           //check number of tabs and fill the entire row
           var $total = navigation.find('li').length;
           $width = 100/$total;
           
           $display_width = $(document).width();
           
           if($display_width < 400 && $total > 3){
               $width = 50;
           }
           navigation.find('li').css('width',$width + '%');
        },
        // Disable the posibility to click on tabs
        /* onTabClick : function(tab, navigation, index){
            
            return false;
        }, */
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#wizard li.active').prevAll().addClass('complete');
            $('#wizard li.active,complete').removeClass('complete');
            $('#wizard .progress-bar').css({width:$percent+'%'});
            var wizard = navigation.closest('.wizard-card');

        }
    })

    /*accordion tab*/
    $('.acor-bayar')
      .on('show.bs.collapse', function(e) {
      $(e.target).prev('.panel-heading').addClass('active');
      })
      .on('hide.bs.collapse', function(e) {
        $(e.target).prev('.panel-heading').removeClass('active');     
    });


    /*accordio tab for paduan bayar*/
       $('.acor-fin-bayar').on('shown.bs.collapse', function(){
          $(this).parent().find(".glyphicon-triangle-bottom").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-top");
       })
       .on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-triangle-top").removeClass("glyphicon-triangle-top").addClass("glyphicon-triangle-bottom");
      });

      /*enable checkbox*/
      $(function() {
        enable_cb();
        $(".cek-cicil").click(enable_cb);
      });

      function enable_cb() {
        if (this.checked) {
            $("input.group1, select.group1").attr("disabled", true);
        } else {
          $("input.group1, select.group1").removeAttr("disabled");
        }
      }


      $('.addOrg').on('shown.bs.modal', function () {
                $('.tutor').show();
      });
      
      $(".close-tool").click(function(){
           $('.tutor').hide();
      });

     /*+- Qty*/
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('#qty').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#qty').val(0);
        }
    });
     
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('#qty').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('#qty').val(0);
        }
    });
    
});

    











