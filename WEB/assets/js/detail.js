$(document).ready(function(){
	/*date picker*/
	$(function(){
		window.prettyPrint && prettyPrint();
		$('#dp1').datepicker({
			format: 'mm-dd-yyyy'
		});
		$('#dp2').datepicker();
		// disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
	});

	/*+- Qty*/
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('#qty').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#qty').val(0);
        }
    });
     $('.qtyplus1').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty1').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('#qty1').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#qty1').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus1").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#qty1').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('#qty1').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('#qty1').val(0);
        }
    });

    /*carousel*/
    $('#det-caro,#det-caro1').carousel({
      interval: 6000
    })

   /*accordio tab for paduan bayar*/
       $('.acor-jadwal').on('shown.bs.collapse', function(){
          $(this).parent().find(".glyphicon-triangle-bottom").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-top");
       })
       .on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-triangle-top").removeClass("glyphicon-triangle-top").addClass("glyphicon-triangle-bottom");
      });

    /*lightbox fasiitas tour*/
    $('.trans-thumb').click(function(){
        $('#mbod').empty();
        var title = $(this).parent('a').attr("title");
        $('#mtit').html(title);
        $($(this).parents('div').html()).appendTo('#mbod');
        $('#transModal').modal({show:true});
    });

    /*lightbox maskapai*/
    $('.trans-thumb1').click(function(){
        $('#mbod1').empty();
        var title = $(this).parent('a').attr("title");
        $('#mtit1').html(title);
        $($(this).parents('div').html()).appendTo('#mbod1');
        $('#transModal1').modal({show:true});
    });
});