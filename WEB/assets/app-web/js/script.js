$(document).ready(function() {
  	   
      $(".onepage").onepage_scroll({
           sectionContainer: "section",     // sectionContainer accepts any kind of selector in case you don't want to use section
           easing: "ease",                  // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in",
                                            // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
           animationTime: 1000,             // AnimationTime let you define how long each section takes to animate
           pagination: true,                // You can either show or hide the pagination. Toggle true for show, false for hide.
           updateURL: false,                // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
           beforeMove: function(index) {},  // This option accepts a callback function. The function will be called before the page moves.
           afterMove: function(index) {},   // This option accepts a callback function. The function will be called after the page moves.
           loop: false,                     // You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.
           keyboard: true,                  // You can activate the keyboard controls
           responsiveFallback: 767,        // You can fallback to normal page scroll by defining the width of the browser in which
                                            // you want the responsive fallback to be triggered. For example, set this to 600 and whenever
                                            // the browser's width is less than 600, the fallback will kick in.
           direction: "vertical"            // You can now define the direction of the One Page Scroll animation. Options available are "vertical" and "horizontal". The default value is "vertical".  
        });
       $('#main-nav').sidr();
       $('.sidr ul li a').click(function() {
         $(".onepage").moveTo($(this).data('index'));
      });
     
  	

      $(window).scroll(function(){
      $('*[class^="prlx"]').each(function(r){
        var pos = $(this).offset().top;
        var scrolled = $(window).scrollTop();
          $('*[class^="prlx"]').css('top', -(scrolled * 0.5) + 'px');     
        });
    });

    $('.sec6 .tab-btn-wrap .nav-tabs > li > a').on('click', function (e) {  
    
        $('.tab-r-isi.tc').css('bottom','-'+$(window).height()+'px');            
        var bottom = $('.tab-r-isi.tc').offset().bottom;
        $(".tab-r-isi.tc").css({bottom:bottom}).animate({"bottom":"0px"}, "10");
        
    });

    /*$('.sec7 .tab-btn-wrap .nav-tabs > li > a').on('click', function (e) {  
    
        $('.tabs-left').css('bottom','-'+$(window).height() / 5 +'px');            
        var bottom = $('.tabs-left').offset().bottom;
        $(".tabs-left").css({bottom:bottom}).animate({"bottom":"0px"}, "10");
        
    });*/
    
    $('.sec8 .carousel').carousel({
      interval: false
    }); 
    $('.carousel-sync').on('slide.bs.carousel', function(ev) {
        var dir = ev.direction == 'right' ? 'prev' : 'next';
      $('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
    });
    $('.carousel-sync').on('slid.bs.carousel', function(ev) {
      $('.carousel-sync').removeClass('sliding');
    });

    $('.carousel-sync1').on('slide.bs.carousel', function(ev) {
        var dir = ev.direction == 'right' ? 'prev' : 'next';
      $('.carousel-sync1').not('.sliding').addClass('sliding').carousel(dir);
    });
    $('.carousel-sync1').on('slid.bs.carousel', function(ev) {
      $('.carousel-sync1').removeClass('sliding');
    });

    

});