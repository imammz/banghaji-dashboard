<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('config/assets_view') ?>
</head>
<body >
	<div class="header "><!--header-->
		<div class="bg1"></div>
		<div class="bg2"></div>
		<div class="navbar-wrapper"><!--navbar wrapper-->
			<!--  
                            Header  --> 
                            <?php 
                            $this->load->view('config/header_view');
                            ?>
			<div class="clearfix"></div>
		</div><!--end navbar wrapper-->
		<div class="clearfix"></div>
		<div class="big-carousel"><!--big carousel-->
			<div id="bigCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#bigCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#bigCarousel" data-slide-to="1"></li>
			    <li data-target="#bigCarousel" data-slide-to="2"></li>
			  </ol>

                          
                          
                          <!-- Slider image, Next lewat database -->
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			      <a href="<?php echo base_url()?>assets/">
			      	<img title="Biaya umroh 2015 terupdate dari Ikhram.com" alt="Biaya umroh 2015 terupdate dari Ikhram.com" src="http://www.ikhram.com/img/banner/umrahmurah-2015-2a.jpg" type="image">
			      </a>
			    </div>
			    <div class="item">
			      <a href="<?php echo base_url()?>assets/">
			      	<img title="Biaya umroh 2015 terupdate dari Ikhram.com" alt="Biaya umroh 2015 terupdate dari Ikhram.com" src="http://www.ikhram.com/img/banner/testimoni-pak-manaf.jpg" type="image">
			      </a>
			    </div>
			    <div class="item">
			      <a href="<?php echo base_url()?>assets/">
			      	<img title="Biaya umroh 2015 terupdate dari Ikhram.com" alt="Biaya umroh 2015 terupdate dari Ikhram.com" src="http://www.ikhram.com/img/banner/mengapa.jpg" type="image">
			      </a>
			    </div>
			  </div>

			  <!-- Controls -->
			  <a class="left carousel-control" href="<?php echo base_url()?>assets/#bigCarousel" role="button" data-slide="prev">
			    <span class="glyphicon  glyphicon-triangle-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="<?php echo base_url()?>assets/#bigCarousel" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>

			</div>
		</div><!--end big carousel-->
                
		<div class="clearfix"></div>
		<div class="container tab-paket"><!--container tab promo & sponsor-->
		    <ul id="Tabs1" class="nav nav-tabs" role="tablist">
		      <li role="presentation" class="active"><a href="#home"  id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Paket Umroh</a></li>
		      <li role="presentation" ><a href="#profile"  role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Spesial Promo</a></li>
		    </ul>
		    <div id="myTabContent" class="tab-content"><!--tab1-->
		      <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
		        <div class="title-cari">Dapatkan perbandingan harga paket umroh murah dengan cepat dan mudah</div>
		        <div class="isi-paket">
                            <form method="post" action="<?php echo site_url('pemesanan/advanced_search/')?>" >
		        	<div class="col-sm-4 step1">
		        		<div class="step-title">
		        			<div class="step-no">1</div>
		        			<div class="step-word">Pilih Tanggal Keberangkatan</div>
		        		</div>
		        		<p class="p-step">Berangkat sekitar tanggal:</p>
		        		<div class="well">
		        			<div class="date-input">
					           <span class="ico-cal"></span><input type="text" value="<?php echo date('d-m-Y')?>" name="tgl_berangkat" class="inp-cal" value="" id="dpd1">
					        </div>
					        <p class="p-step psh-768">Pulang sekitar tanggal:</p>
					        <div class="date-input"> 
					            <span class="ico-cal"></span><input type="text" value="<?php echo date('d-m-Y')?>" name="tgl_pulang" class="inp-cal" value="" id="dpd2">
					        </div>
				        </div>
		        	</div>
		        	<div class="col-sm-2 step2">
		        		<div class="step-title">
		        			<div class="step-no">2</div>
		        			<div class="step-word">Pilih Daerah Keberangkatan</div>
		        		</div>
		        		<p class="p-step">Berangkat dari:</p>
                                        
		        		<select  class="form-control select-css">
					
                                        <?php foreach($provinsi as $row) { ?>
                                            <option>Jakarta - CGK</option>
                                        <?php  } ?>
                                            
					</select>
		        	</div>
		        	<div class="col-sm-2 step4">
		        		<div class="step-title">
		        			<div class="step-no">3</div>
		        			<div class="step-word">Pilih Harga</div>
		        		</div>
		        		<div class="clearfix"></div>
	        			<p class="p-step">Biaya Umroh :</p>
		        		<select class="form-control select-css">
						  <option><= $2000</option>
						  <option>$2001 - $2500</option>
						  <option>$2501 - $3000</option>
						  <option>$3001 - $3500</option>
						  <option>$3501 - $4000</option>
						  <option><= $4000</option>
						</select>
		        	</div>
		        	<div class="col-sm-4 step3">
		        		<div class="step-title">
		        			<div class="step-no">4</div>
		        			<div class="step-word">Cari Paket</div>
		        		</div>
		        		<div class="clearfix"></div>
	        			<div class="qty">
	        				<div class="qty-l">
							    <input type='text' name='quantity' value='1' class='input-css fl' id="qty" />
							</div>
							<div class="qty-r">
								<input type='button' value='+' class='qtyplus' field='quantity' />
								<input type='button' value='-' class='qtyminus' field='quantity' />
							</div>
	        				<p class="p-step">Dewasa</p>
	        			</div>
	        			<div class="qty">
		        			<div class="qty-l">
								    <input type='text' name='quantity' value='1' class='input-css fl' id="qty1"/>
								</div>
							<div class="qty-r">
								<input type='button' value='+' class='qtyplus1' field='quantity' />
								<input type='button' value='-' class='qtyminus1' field='quantity' />
							</div>
	        				<p class="p-step">Anak-Anak</p>
	        			</div>
	        			<button class="cari-paket hvr-radial-out">Cari Paket Terbaik</button>
		        	</div>
                            </form>
		        </div>
		      </div>
		      <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
		      	<div class="isi-paket">
		      		<div class="no-more-tables">
					    	<table class="table table-responsive table-spesial">
							    <tbody>
							      	<tr>
									    <td class="prom-cat"  style="vertical-align:middle">
									    	<div class="logo-air">
									    		<p>Spesial Promo Dari</p>
									    		<div class="logo-air-img"><img src="<?php echo base_url()?>assets/img/bca.png"></div>
									    	</div>
									    </td>
								    	<td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
								  </tr>
								  <tr>
								  	<td class="prom-cat"  style="vertical-align:middle">
								    	<div class="logo-air">
								    		<p>Spesial Promo Dari</p>
								    		<div class="logo-air-img"><img src="<?php echo base_url()?>assets/img/bca.png"></div>
								    	</div>
								    </td>
								    <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
								  </tr>
								  <tr>
								  	<td class="prom-cat"  style="vertical-align:middle">
								    	<div class="logo-air">
								    		<p>Spesial Promo Dari</p>
								    		<div class="logo-air-img"><img src="<?php echo base_url()?>assets/img/bca.png"></div>
								    	</div>
								    </td>
								    <td  class="bd-bot" data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  class="bd-bot" data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  class="bd-bot" data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  class="bd-bot" data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  class="bd-bot" data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  class="bd-bot" data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  class="bd-bot" data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
								  </tr>
								  <tr>
									    <td class="prom-cat"  style="vertical-align:middle">
									    	<div class="logo-air">
									    		<p>Spesial Promo Dari</p>
									    		<div class="logo-air-img"><img src="<?php echo base_url()?>assets/img/bni.png"></div>
									    	</div>
									    </td>
								    	<td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
								  </tr>
								  <tr>
								  	<td class="prom-cat"  style="vertical-align:middle">
								    	<div class="logo-air">
								    		<p>Spesial Promo Dari</p>
								    		<div class="logo-air-img"><img src="<?php echo base_url()?>assets/img/bni.png"></div>
								    	</div>
								    </td>
								    <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
								  </tr>
								  <tr class="last-tr">
								  	<td class="prom-cat"  style="vertical-align:middle">
								    	<div class="logo-air">
								    		<p>Spesial Promo Dari</p>
								    		<div class="logo-air-img"><img src="<?php echo base_url()?>assets/img/bni.png"></div>
								    	</div>
								    </td>
								    <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
								  </tr>


							    </tbody>
							</table>
				      	</div>
					</div>
		      </div><!--end tab-->
		    </div>
		</div><!--container tab promo & sponsor-->
	</div><!--end header-->
	<div class="clearfix"></div>
	<div class="home-wrap"><!--home-wrap-->
		<div class="container">
			<div class="hot-paket">
				<h2>Hot Paket</h2>
				<div class="hot-paket-list">
					<div class="col-sm-3 pl-0 hot-box ">
						<div class="hot-list hvr-shrink">
							<div class="hot-img">
								<img  src="http://cdn01.tiket.photos/img/business/s/i/business-singapore_night_safari_home_banner1.l.jpg" alt="Singapore Night Safari" title="Singapore Night Safari">
							</div>
							<div class="hot-isi">
								<h3>Hot Promo dari Bank BCA</h3>
								<p>Lorem ipsum dolor sitamet, consecteturadipisicing elit, sed do</p>
								<a href="<?php echo base_url()?>assets/">Lebih Detail</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3 hot-box">
						<div class="hot-list hvr-shrink">
							<div class="hot-img">
								<img  src="http://cdn01.tiket.photos/img/business/s/i/business-singapore_night_safari_home_banner1.l.jpg" alt="Singapore Night Safari" title="Singapore Night Safari">
							</div>
							<div class="hot-isi">
								<h3>Hot Promo dari Bank BCA</h3>
								<p>Lorem ipsum dolor sitamet, consecteturadipisicing elit, sed do</p>
								<a href="<?php echo base_url()?>assets/">Lebih Detail</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3 hot-box ">
						<div class="hot-list hvr-shrink">
							<div class="hot-img">
								<img  src="http://cdn01.tiket.photos/img/business/s/i/business-singapore_night_safari_home_banner1.l.jpg" alt="Singapore Night Safari" title="Singapore Night Safari">
							</div>
							<div class="hot-isi">
								<h3>Hot Promo dari Bank BCA</h3>
								<p>Lorem ipsum dolor sitamet, consecteturadipisicing elit, sed do</p>
								<a href="<?php echo base_url()?>assets/">Lebih Detail</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3 hot-box pr-0 ">
						<div class="hot-list hvr-shrink">
							<div class="hot-img">
								<img  src="http://cdn01.tiket.photos/img/business/s/i/business-singapore_night_safari_home_banner1.l.jpg" alt="Singapore Night Safari" title="Singapore Night Safari">
							</div>
							<div class="hot-isi">
								<h3>Hot Promo dari Bank BCA</h3>
								<p>Lorem ipsum dolor sitamet, consecteturadipisicing elit, sed do</p>
								<a href="<?php echo base_url()?>assets/">Lebih Detail</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			
			<div class="paket-slide"><!--paket-slide-->
				<div id="paketSlide" class="carousel slide" data-ride="carousel">
				  <div class="blank"></div>
				  <div class="control-wrap">
				  	<!-- Controls -->
					  <a class="left carousel-control" href="<?php echo base_url()?>assets/#paketSlide" role="button" data-slide="prev">
					    <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="right carousel-control" href="<?php echo base_url()?>assets/#paketSlide" role="button" data-slide="next">
					    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
					  <div class="clearfix"></div>
				  </div>
				   <div class="blank-r"></div>
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">

				    	 <div class="judul-promo">
					    	<h2>
					    		Paket Terpopuler
					    		<span>Desember 2015</span>
					    	</h2>
				    	</div>
				    	<div class="no-more-tables">
					    	<table class="table table-responsive table-promo">
							    <tbody>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        
							    </tbody>
							</table>
				      	</div>
				    </div>
				    <div class="item">
				      	<div class="judul-promo">
					    	<h2>
					    		Paket Terpopuler
					    		<span>Maret 2016</span>
					    	</h2>
				    	</div>
				    	<div id="no-more-tables">
					    	<table class="table table-responsive table-promo">
							    <tbody>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        
							    </tbody>
							</table>
				      	</div>
				    </div>
				    <div class="item">
				      	<div class="judul-promo">
					    	<h2>
					    		Paket Terpopuler
					    		<span>Febuari 2016</span>
					    	</h2>
				    	</div>
				    	<div id="no-more-tables">
					    	<table class="table table-responsive table-promo">
							    <tbody>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        <tr>
							            <td  data-title="Tanggal">
							                <div class="tgl">
								          		20</br>
								          		<span>Agt'15</span>
								          	</div>
							            </td>
							            <td  data-title="Durasi">
							                <div class="fromTo">
								          		JKT - JED
								          		<p><span>12</span>Hari Perjalanan</p>
								          	</div>
							            </td>
							            <td  data-title="Travel">
							                <div class="logo-mas">
								          		<img src="<?php echo base_url()?>assets/img/logo-mas3.png">
								          	</div>
							            </td>
							            <td  data-title="Fasilitas">
							            	<div class="fasilitas">
								          		<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>4
								          			</div>
								          			<div class="rate">
								          				<i class="glyphicon glyphicon-star"></i>5
								          			</div>
								          		</a>
								          	</div>
							            </td>
								        <td  data-title="Sisa Kuota">
								        	<div class="kuota">
								          		<span>9</span>Pax Lagi
								          	</div>
						          		</td>
							            <td  data-title="Biaya">
							            	<div class="biaya">
								          		<div class="biaya-l">
								          			<p class="old-price">Rp 24.3jt</p>
								          			<div class="new-price"><p>Rp</p><span>18jt</span></div>
								          		</div>
								          		<div class="ask">
								          			<a data-title="&lt;div width='100%' style='text-align:center; font-size:13px;'&gt;MAU DISKON TAMBAHAN??&lt;/div&gt;&lt;b&gt;&lt;i class='&lt;div style='text-align:center; '&gt;Hubungi &lt;span  font-size:13px;'&gt;&lt;b&gt;CS BangHaji.com&lt;/b&gt;&lt;/span&gt;&lt;br&gt;&lt;div class='clearfix' style='padding-bottom:5px;'&gt;&lt;/div&gt;&lt;table border='0' width='100%' style=' padding-top:5px; '&gt;&lt;tr&gt;&lt;td style='padding-top:0px; padding-left:30px; padding-bottom:5px;'&gt;&lt;i class='fa fa-phone-square'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='padding-top:5px; text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;031-8479-331&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;'&gt;&lt;i class='fa fa-whatsapp'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0822-311-20002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-right:10px;padding-bottom:5px;'&gt;0878-538-00002&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='padding-left:30px;padding-bottom:5px;'&gt;&lt;i class='fa fa-mobile'&gt;&lt;/i&gt;&lt;/td&gt;&lt;td style='text-align:left !important; padding-left:10px; padding-bottom:10px;padding-right:10px;'&gt;0857-647-08000&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;" data-html="true" data-placement="right"  rel="tooltip" href="<?php echo base_url()?>assets/#">
								          				<div class="ask-ico">?</div>
								          			</a>
								          		</div>
								          		<div class="clearfix"></div>
								          	</div>
							            </td>
							            <td  data-title="Pesan">
							                <div class="book-now">
								          		<a href="<?php echo base_url()?>assets/" class="hvr-shutter-out-vertical">Detail Promosi</a>
								          	</div>
							            </td>
							        </tr>
							        <div class="clearfix"></div>
							        
							    </tbody>
							</table>
				      	</div>
				    </div>
				    
				  </div>
				  <!-- Indicators -->
				 <ol class="carousel-indicators" >
				    <li data-target="#paketSlide" data-slide-to="0" class="active"></li>
				    <li data-target="#paketSlide" data-slide-to="1"></li>
				    <li data-target="#paketSlide" data-slide-to="2"></li>
				  </ol>
				  
				</div>
			</div><!-- end paket-slide-->
			<div class="clearfix"></div>
			<div class="home-isi animatedParent"><!-- home-isi-->
				<div class="col-sm-5  home-left animated fadeInLeftShort">
					<h2>Paket Terbaru</h2>
					<div class="paket-list hvr-grow">
						<div class="list-atas">
							<div class="col-sm-9 pl-0">
								<div class="list-tgl">
									<p>20</p>
									<span>Agt'15</span>
								</div>
								<div class="list-ket">
									<p>JKT - JED</p>
									<div class="list-fasilitas">
										<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="bottom"  rel="tooltip" href="<?php echo base_url()?>assets/#">
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>4
						          			</div>
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>5
						          			</div>
						          		</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 fr">
								<div class="list-logo">
									<img src="<?php echo base_url()?>assets/img/logo-mas.png">
								</div>
							</div>
						</div>
						<div class="list-bwh">
							<div class="col-sm-9 pl-0">
								<p class="l-old-price">Rp24.3jt</p>
								<p class="l-new-price">Rp20.3jt</p>
							</div>
							<div class="col-sm-3 fr">
								<p class="tour-name">Maktour</p>
							</div>
						</div>
					</div>
					<div class="paket-list hvr-grow">
						<div class="list-atas">
							<div class="col-sm-9 pl-0">
								<div class="list-tgl">
									<p>20</p>
									<span>Agt'15</span>
								</div>
								<div class="list-ket">
									<p>JKT - JED</p>
									<div class="list-fasilitas">
										<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="bottom"  rel="tooltip" href="<?php echo base_url()?>assets/#">
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>4
						          			</div>
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>5
						          			</div>
						          		</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 fr">
								<div class="list-logo">
									<img src="<?php echo base_url()?>assets/img/logo-mas.png">
								</div>
							</div>
						</div>
						<div class="list-bwh">
							<div class="col-sm-9 pl-0">
								<p class="l-old-price">Rp24.3jt</p>
								<p class="l-new-price">Rp20.3jt</p>
							</div>
							<div class="col-sm-3 fr">
								<p class="tour-name">Maktour</p>
							</div>
						</div>
					</div>
					<div class="paket-list hvr-grow">
						<div class="list-atas">
							<div class="col-sm-9 pl-0">
								<div class="list-tgl l-red">
									<p>20</p>
									<span>Agt'15</span>
								</div>
								<div class="list-ket">
									<p>JKT - JED - SBY</p>
									<div class="list-fasilitas">
										<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="bottom"  rel="tooltip" href="<?php echo base_url()?>assets/#">
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>4
						          			</div>
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>5
						          			</div>
						          		</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 fr">
								<div class="list-logo">
									<img src="<?php echo base_url()?>assets/img/logo-mas.png">
								</div>
							</div>
						</div>
						<div class="list-bwh">
							<div class="col-sm-9 pl-0">
								<p class="l-old-price">Rp24.3jt</p>
								<p class="l-new-price">Rp20.3jt</p>
							</div>
							<div class="col-sm-3 fr">
								<p class="tour-name">Madani</p>
							</div>
						</div>
					</div>
					<div class="paket-list hvr-grow">
						<div class="list-atas">
							<div class="col-sm-9 pl-0">
								<div class="list-tgl">
									<p>20</p>
									<span>Agt'15</span>
								</div>
								<div class="list-ket">
									<p>JKT - JED</p>
									<div class="list-fasilitas">
										<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="bottom"  rel="tooltip" href="<?php echo base_url()?>assets/#">
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>4
						          			</div>
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>5
						          			</div>
						          		</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 fr">
								<div class="list-logo">
									<img src="<?php echo base_url()?>assets/img/logo-mas.png">
								</div>
							</div>
						</div>
						<div class="list-bwh">
							<div class="col-sm-9 pl-0">
								<p class="l-old-price">Rp24.3jt</p>
								<p class="l-new-price">Rp20.3jt</p>
							</div>
							<div class="col-sm-3 fr">
								<p class="tour-name">Ramani</p>
							</div>
						</div>
					</div>
					<div class="paket-list hvr-grow">
						<div class="list-atas">
							<div class="col-sm-9 pl-0">
								<div class="list-tgl">
									<p>20</p>
									<span>Agt'15</span>
								</div>
								<div class="list-ket">
									<p>JKT - JED</p>
									<div class="list-fasilitas">
										<a title="&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Mekkah:&lt;/b&gt; Nawarat Syam/Final Rehab ( rating : N/A )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Mekkah: &lt;/b&gt;3&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-home'&gt;&lt;/i&gt; Hotel Madinah:&lt;/b&gt; Mubarak Al Madinah Hotel/Safir Al Saha Hotel ( rating : 6.1 )&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-star'&gt;&lt;/i&gt; Bintang Hotel Madinah:&lt;/b&gt; 4&lt;br&gt;&lt;b&gt;&lt;i class='glyphicon glyphicon-plane'&gt;&lt;/i&gt; Maskapai:&lt;/b&gt; Saudi Airlines" data-html="true" data-placement="bottom"  rel="tooltip" href="<?php echo base_url()?>assets/#">
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>4
						          			</div>
						          			<div class="list-rate">
						          				<i class="glyphicon glyphicon-star"></i>5
						          			</div>
						          		</a>
									</div>
								</div>
							</div>
							<div class="col-sm-3 fr">
								<div class="list-logo">
									<img src="<?php echo base_url()?>assets/img/logo-mas.png">
								</div>
							</div>
						</div>
						<div class="list-bwh">
							<div class="col-sm-9 pl-0">
								<p class="l-new-price">Rp20.3jt</p>
							</div>
							<div class="col-sm-3 fr">
								<p class="tour-name">Patuna</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="run-pack">
						<h3>Travel Package</h3>
						<div class="run-box hvr-ripple-out">
							<div class="col-sm-7 pl-0 run-l">
								<div class="run-tgl">
									<p>20</p>
									<span>Dec' 2015</span>
								</div>
	          					<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
								<div class="run-tgl pl-0">
									<p>30</p>
									<span>Dec' 2015</span>
								</div>
							</div>
							<div class=" col-sm-5 pl-0 run-r">
								<h4>Maktour</h4>
								<p>Ust Abdullah Gymnastyar</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-7 home-right animated fadeInRightShort">
					<h2>Pencairan Cepat Promo</h2>
					<p>Lihat harga termurah dari hasil pencarian selama 48 jam terakhir di sini. Klik tanggal untuk
memesan atau melihat lebih jauh.</p>
					<div class="clearfix"></div>
					<div class="choose-date">
							<div class="col-sm-3">
								<h4>Bulan:</h4>
								<select class="form-control select-css1">
								  <option>Januari</option>
								  <option>Februari</option>
								  <option>Maret</option>
								  <option>April</option>
								  <option>Mei</option>
								  <option>Juni</option>
								  <option>Juli</option>
								  <option>Agustus</option>
								  <option>September</option>
								  <option>Oktober</option>
								  <option>November</option>
								  <option>Desember</option>
								</select>	
							</div>
							<div class="col-sm-3">
								<h4>Dari:</h4>
								<select class="form-control select-css1">
									<option>Bali / Denpasar	</option>
									<option>Jakarta	</option>
									<option>Makassar</option>	
									<option>Medan</option>
									<option>Surabaya</option>
									<option>Yogyakarta</option>
								</select>	
							</div>
							<div class="col-sm-3">
								<h4>Hotel Madinnah:</h4>
								<select class="form-control select-css1">
									<option>Semua</option>
									<option>Hotel Bintang 5</option>
									<option>Hotel Bintang 4</option>
									<option>Hotel Bintang 3</option>	
								</select>	
							</div>
							<div class="col-sm-3">
								<h4>Hotel Mekkah:</h4>
								<select class="form-control select-css1">
									<option>Semua</option>
									<option>Hotel Bintang 5</option>
									<option>Hotel Bintang 4</option>
									<option>Hotel Bintang 3</option>	
								</select>	
							</div>
					</div>
					<div class="clearfix"></div>
					<div class='calendar'></div>
				</div>
				<div class="clearfix"></div>
			</div><!-- end home-isi-->
		</div>
	</div><!--end home-wrap-->
	<div class="clearfix"></div>
	<div class="ustad-wrap animatedParent"><!-ustad-wrap-->
		<div class="container ustad animated fadeInDownShort">
			<h2>Ustad Blog</h2>	
			<div class="ustad-slide">
				<div id="ustadSlide" class="carousel slide" data-ride="carousel">
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
					    <div class="item active">
					      	<div class="col-sm-4 pl-0 ustad-list">
					      		<div class="ustad-box hvr-float">
						      		<div class="ustad-up">
						      			<div class="ust-blog-img"><img src="<?php echo base_url()?>assets/img/testi.png"></div>
							      		<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							      		<div class="clearfix"></div>
						      			<div class="ustad-l">
							      			<h4>Ust. Abdullah Gymnastyar</h4>
						      			</div>
						      			<div class="ustad-r">
						      				<p>12 Nov 2015</p>
						      			</div>
						      		</div>
						      		<div class="ustad-down">
						      			<p>orem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor nulla id sagittis ullamcorper. Vivamus eget erat efficitur, semper risus vel, semper dolor. Nam arcu metus, consequat eu malesuada eu, elementum non risus. Fusce et ante turpis. Curabitur vitae enim nisl. Ut quis lorem quis orci ultricies mattis nec in lectus. Phasellus arcu nisl, rutrum nec blandit eget, scelerisque vitae lectus. .</p>
						      			<a href="<?php echo base_url()?>assets/">Lebih Detail <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
						      		</div>
					      		</div>
					      	</div>
					      	<div class="col-sm-4 ustad-list">
					      		<div class="ustad-box hvr-float">
						      		<div class="ustad-up">
						      			<div class="ust-blog-img"><img src="<?php echo base_url()?>assets/img/testi.png"></div>
							      		<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							      		<div class="clearfix"></div>
						      			<div class="ustad-l">
							      			<h4>Ust. Abdullah Gymnastyar</h4>
						      			</div>
						      			<div class="ustad-r">
						      				<p>12 Nov 2015</p>
						      			</div>
						      		</div>
						      		<div class="ustad-down">
						      			<p>orem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor nulla id sagittis ullamcorper. Vivamus eget erat efficitur, semper risus vel, semper dolor. Nam arcu metus, consequat eu malesuada eu, elementum non risus. Fusce et ante turpis. Curabitur vitae enim nisl. Ut quis lorem quis orci ultricies mattis nec in lectus. Phasellus arcu nisl, rutrum nec blandit eget, scelerisque vitae lectus. .</p>
						      			<a href="<?php echo base_url()?>assets/">Lebih Detail <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
						      		</div>
					      		</div>
					      	</div>
					      	<div class="col-sm-4 pr-0 ustad-list">
					      		<div class="ustad-box hvr-float">
						      		<div class="ustad-up">
						      			<div class="ust-blog-img"><img src="<?php echo base_url()?>assets/img/testi.png"></div>
							      		<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							      		<div class="clearfix"></div>
						      			<div class="ustad-l">
							      			<h4>Ust. Abdullah Gymnastyar</h4>
						      			</div>
						      			<div class="ustad-r">
						      				<p>12 Nov 2015</p>
						      			</div>
						      		</div>
						      		<div class="ustad-down">
						      			<p>orem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor nulla id sagittis ullamcorper. Vivamus eget erat efficitur, semper risus vel, semper dolor. Nam arcu metus, consequat eu malesuada eu, elementum non risus. Fusce et ante turpis. Curabitur vitae enim nisl. Ut quis lorem quis orci ultricies mattis nec in lectus. Phasellus arcu nisl, rutrum nec blandit eget, scelerisque vitae lectus. .</p>
						      			<a href="<?php echo base_url()?>assets/">Lebih Detail <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
						      		</div>
					      		</div>
					      	</div>
					    </div>
					    <div class="item">
					      	<div class="col-sm-4 pl-0 ustad-list">
					      		<div class="ustad-box hvr-float">
						      		<div class="ustad-up">
						      			<div class="ust-blog-img"><img src="<?php echo base_url()?>assets/img/testi.png"></div>
							      		<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							      		<div class="clearfix"></div>
						      			<div class="ustad-l">
							      			<h4>Ust. Abdullah Gymnastyar</h4>
						      			</div>
						      			<div class="ustad-r">
						      				<p>12 Nov 2015</p>
						      			</div>
						      		</div>
						      		<div class="ustad-down">
						      			<p>orem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor nulla id sagittis ullamcorper. Vivamus eget erat efficitur, semper risus vel, semper dolor. Nam arcu metus, consequat eu malesuada eu, elementum non risus. Fusce et ante turpis. Curabitur vitae enim nisl. Ut quis lorem quis orci ultricies mattis nec in lectus. Phasellus arcu nisl, rutrum nec blandit eget, scelerisque vitae lectus. .</p>
						      			<a href="<?php echo base_url()?>assets/">Lebih Detail <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
						      		</div>
					      		</div>
					      	</div>
					      	<div class="col-sm-4 ustad-list">
					      		<div class="ustad-box hvr-float">
						      		<div class="ustad-up">
						      			<div class="ust-blog-img"><img src="<?php echo base_url()?>assets/img/testi.png"></div>
							      		<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							      		<div class="clearfix"></div>
						      			<div class="ustad-l">
							      			<h4>Ust. Abdullah Gymnastyar</h4>
						      			</div>
						      			<div class="ustad-r">
						      				<p>12 Nov 2015</p>
						      			</div>
						      		</div>
						      		<div class="ustad-down">
						      			<p>orem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor nulla id sagittis ullamcorper. Vivamus eget erat efficitur, semper risus vel, semper dolor. Nam arcu metus, consequat eu malesuada eu, elementum non risus. Fusce et ante turpis. Curabitur vitae enim nisl. Ut quis lorem quis orci ultricies mattis nec in lectus. Phasellus arcu nisl, rutrum nec blandit eget, scelerisque vitae lectus. .</p>
						      			<a href="<?php echo base_url()?>assets/">Lebih Detail <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
						      		</div>
					      		</div>
					      	</div>
					      	<div class="col-sm-4 pr-0 ustad-list">
					      		<div class="ustad-box hvr-float">
						      		<div class="ustad-up">
						      			<div class="ust-blog-img"><img src="<?php echo base_url()?>assets/img/testi.png"></div>
							      		<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							      		<div class="clearfix"></div>
						      			<div class="ustad-l">
							      			<h4>Ust. Abdullah Gymnastyar</h4>
						      			</div>
						      			<div class="ustad-r">
						      				<p>12 Nov 2015</p>
						      			</div>
						      		</div>
						      		<div class="ustad-down">
						      			<p>orem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor nulla id sagittis ullamcorper. Vivamus eget erat efficitur, semper risus vel, semper dolor. Nam arcu metus, consequat eu malesuada eu, elementum non risus. Fusce et ante turpis. Curabitur vitae enim nisl. Ut quis lorem quis orci ultricies mattis nec in lectus. Phasellus arcu nisl, rutrum nec blandit eget, scelerisque vitae lectus. .</p>
						      			<a href="<?php echo base_url()?>assets/">Lebih Detail <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
						      		</div>
					      		</div>
					      	</div>
					    </div>
				  </div>
				  <a class="ustad-lain hvr-grow" href="<?php echo base_url()?>assets/">Lihat Ustad Blog Selengkapnya</a>
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#ustadSlide" data-slide-to="0" class="active"></li>
				    <li data-target="#ustadSlide" data-slide-to="1"></li>
				  </ol>
				</div><!--end test slide-->
			</div>
		</div>
	</div><!--end ustad-wrap-->
	<div class="clearfix"></div>
	<div class="alasan-wrap animatedParent"><!--alasan-wrap-->
		<div class="container alasan animated fadeInUpShort">
			<h2>5 Alasan kenapa memilih Bang Haji</h2>
			<div class="alasan-point">
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="<?php echo base_url()?>assets/img/reason1a.png">
					</div>
					<div class="alasan-no">1</div>
					<h3>Partner Resmi Departemen Agama</h3>
					<p>Lorem ipsum dolor sitamet, consecteturadipisicing elit, sed do</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="<?php echo base_url()?>assets/img/reason2a.png">
					</div>
					<div class="alasan-no">2</div>
					<h3>Pencarian Terlengkap</h3>
					<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="<?php echo base_url()?>assets/img/reason3a.png">
					</div>
					<div class="alasan-no">3</div>
					<h3>Dijamin Harga Terbaik</h3>
					<p>Amet, consectetu adipisicing elit, sed do eiusmod tempor</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="<?php echo base_url()?>assets/img/reason4a.png">
					</div>
					<div class="alasan-no">4</div>
					<h3>Harga sudah termasuk pajak</h3>
					<p>Labore et dolore magna aliqua. Ut enim ad minim veniam</p>
				</div>
				<div class="alasan-col">
					<div class="alasan-img hvr-push">
						<img src="<?php echo base_url()?>assets/img/reason5a.png">
					</div>
					<div class="alasan-no">5</div>
					<h3>Transaksi Online Aman</h3>
					<p>Quis nostrud exercitation ullamcolaboris nisi ut aliquip ex</p>
				</div>
			</div>
		</div>
	</div><!--end alasan-wrap-->
	<div class="clearfix"></div>
	<div class="partner-wrap animatedParent"><!--partner-wrap-->
		<div class="container partner animated fadeIn">
			<h2>Partner Travel Haji dan Umroh</h2>
			<div id="partnerSlide" class="carousel slide" data-ride="carousel">
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			    	<ul class="partner-list">
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour2.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour3.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour4.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour5.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour6.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour7.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour8.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour9.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour2.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour3.jpg"></li>
					</ul> 
			    </div>
			    <div class="item">
			    	<ul class="partner-list">
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour2.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour3.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour4.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour5.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour6.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour7.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour8.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour9.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour2.jpg"></li>
						<li class="hvr-bob"><img src="<?php echo base_url()?>assets/img/tour3.jpg"></li>
					</ul>
			    </div>
			    <div class="item">
			    	<ul class="partner-list">
						<li><img src="<?php echo base_url()?>assets/img/tour.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour2.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour3.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour4.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour5.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour6.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour7.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour8.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour9.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour2.jpg"></li>
						<li><img src="<?php echo base_url()?>assets/img/tour3.jpg"></li>
					</ul> 
			    </div>
			  </div>
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#partnerSlide" data-slide-to="0" class="active"></li>
			    <li data-target="#partnerSlide" data-slide-to="1"></li>
			    <li data-target="#partnerSlide" data-slide-to="2"></li>
			  </ol>
			  
			</div>	
			<a href="<?php echo base_url()?>assets/" class="hvr-grow">Lihat Partner Lainnya</a>
		</div>
	</div><!--end partner-wrap-->
	<div class="clearfix"></div>
	<div class="testi-wrap animatedParent"><!--testi-wrap-->
		<div class="container testi animated fadeInDownShort">
			<h2>Apa kata mereka tentang Bang Haji?</h2>	
			<div class="test-slide">
				<div id="testSlide" class="carousel slide" data-ride="carousel">
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
					    <div class="item active">
					      	<div class="col-sm-6 ">
					      		<div class="testi-full">
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      		</div>
					      		<div class="testi-l">
					      			<img src="http://www.multivisual.nl/img/avatar.jpg">
					      		</div>
					      		<div class="testi-r ">
					      			<span>Ibu Tika Mastika ratu</span>
					      			<p>Umroh Maktour 2015</p>
					      		</div>
					      	</div>
					      	<div class="col-sm-6 ">
					      		<div class="testi-full">
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      		</div>
					      		<div class="testi-l">
					      			<img src="http://www.multivisual.nl/img/avatar.jpg">
					      		</div>
					      		<div class="testi-r ">
					      			<span>Ibu Tika Mastika ratu</span>
					      			<p>Umroh Maktour 2015</p>
					      		</div>
					      	</div>
					    </div>
					    <div class="item">
					      	<div class="col-sm-6 ">
					      		<div class="testi-full">
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      		</div>
					      		<div class="testi-l">
					      			<img src="http://www.multivisual.nl/img/avatar.jpg">
					      		</div>
					      		<div class="testi-r ">
					      			<span>Ibu Tika Mastika ratu</span>
					      			<p>Umroh Maktour 2015</p>
					      		</div>
					      	</div>
					      	<div class="col-sm-6 ">
					      		<div class="testi-full">
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      		</div>
					      		<div class="testi-l">
					      			<img src="http://www.multivisual.nl/img/avatar.jpg">
					      		</div>
					      		<div class="testi-r ">
					      			<span>Ibu Tika Mastika ratu</span>
					      			<p>Umroh Maktour 2015</p>
					      		</div>
					      	</div>
					    </div>
					    <div class="item">
					      	<div class="col-sm-6 ">
					      		<div class="testi-full">
					      			<p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<i class="fa fa-quote-right"></i></p>
					      		</div>
					      		<div class="testi-l">
					      			<img src="http://www.multivisual.nl/img/avatar.jpg">
					      		</div>
					      		<div class="testi-r ">
					      			<span>Ibu Tika Mastika ratu</span>
					      			<p>Umroh Maktour 2015</p>
					      		</div>
					      	</div>
					      	
					    </div>
				  </div>
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#testSlide" data-slide-to="0" class="active"></li>
				    <li data-target="#testSlide" data-slide-to="1"></li>
				    <li data-target="#testSlide" data-slide-to="2"></li>
				  </ol>
				</div><!--end test slide-->
			</div>
		</div>
	</div><!--end testi-wrap-->
	<div class="clearfix"></div>
	<?php $this->load->view('config/footer_view'); ?>
</body>
</html>