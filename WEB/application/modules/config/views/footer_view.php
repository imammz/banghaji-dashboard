<div class="rss-wrap animatedParent"><!--end rss-wrap-->
    <div class="container rss animated pulse">
        <h2>Daftarkan email Anda untuk info tiket PROMO, hotel dan semua diskon spesial Traveloka hingga 70%, GRATIS</h2>
        <div class="rss-isi">	
            <input type="text" placeholder="Daftarkan email Anda di sini"  value="" id="textfield"  class="hvr-grow">
            <button type="button" class="hvr-radial-out">Daftar</button>
            <div class="clearfix"></div>
        </div>
    </div>
</div><!--end rss-wrap-->
<div class="clearfix"></div>
<div class="footer-wrap animatedParent">
    <div class="container footer">	
        <div class="footer-atas  animated fadeIn">
            <div class="footer-list">
                <div class="foot-logo"><img src="<?php echo base_url() ?>assets/img/logo-foot.png" class="img-responsive"></div>
                <p>Transaksi Online Anda Dijamin Aman</p>
                <div class="secure-logo"></div>
            </div>
            <div class="footer-list">
                <h4>Tentang Kami</h4>
                <ul>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Cara Memesan</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Hubungi Kami</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">FAQ</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Karir</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Sitemap</a></li>
                </ul>
            </div>
            <div class="footer-list">
                <h4>Lainnya</h4>
                <ul>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Cek Pesanan</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Konfirmasi Pembayaran</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Syarat & Ketentuan</a></li>
                    <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow">Kebijakan Privasi</a></li>
                </ul>
            </div>
            <div class="footer-list cs">
                <ul class="cust-sup">
                    <div class="col-sm-7 pl-0"> 
                        <h4>Customer Support</h4>
                        <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow"><i class="fa fa-phone"></i>+62 212963 3600</a></li>
                        <li><a href="<?php echo base_url() ?>assets/" class="hvr-grow"><i class="fa fa-envelope-o"></i>cs@banghaji.com</a></li>
                        <ul class="socmed">
                            <li class="fb" class="hvr-float"><a href="<?php echo base_url() ?>assets/"><i class="fa fa-facebook"></i></a></li>
                            <li class="gp"class="hvr-float"><a href="<?php echo base_url() ?>assets/"><i class="fa fa-google-plus"></i></a></li>
                            <li class="tw" class="hvr-float"><a href="<?php echo base_url() ?>assets/"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-5 pr-0 hi-767">
                        <img src="<?php echo base_url() ?>assets/img/cs1.png">
                    </div>
                    <div class="clearfix"></div>
                </ul>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="footer-download animatedParent">
            <div class="col-sm-4 pl-0 col-md-offset-1 animated fadeInUpShort">
                <div class="img-phone hvr-pop">
                    <img src="<?php echo base_url() ?>assets/img/phone.png" alt="">
                </div>
            </div>
            <div class="col-sm-7 pr-0 animated fadeInUpShort">
                <p class="mob-title"><b>Download GRATIS Bang Haji - Aplikasi Booking Tiket Pesawat dan Hotel sekarang juga!</b></p>
                <div class="clearfix"></div>
                <div class="dl-app"><a href="https://app.adjust.com/dk6ct2" target="_blank"><img src="<?php echo base_url() ?>assets/img/goplay.png"></a></div>
                <div class="dl-app"><a href="https://app.adjust.com/wkffwc" target="_blank"><img src="<?php echo base_url() ?>assets/img/apstore.png"></a></div>
                <div class="clearfix"></div>
                <p>Bang Haji Tiket Pesawat dan Hotel booking App dapat didownload di Google Play dan iTunes App store untuk gadget/hp Anda</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="footer-bawah">
            <p class="">Copyright &copy; 2015 Bang Haji </p>
        </div>
    </div>
</div>
<div class="chat-wrap">
    <div class="cust-button">
        <a href="<?php echo base_url() ?>assets/#">
            <div class="fl cust-button-l">
                <span>Butuh Bantuan?</span>
                <p>Tinggalkan Pesan</p>
            </div>
            <div class="fr cust-button-r"></div>
        </a>
    </div>
    <div class="msg-wrap">
        <div class="msg-head">
            <p>Banghaji.com Live Chat</p>
            <a href="<?php echo base_url() ?>assets/#" class="cls-msg fr"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
        </div>
        <div class="msg-isi">
        </div>
    </div>
</div>




<div class="promo-button">
    <div data-toggle="modal" data-target=".modal-pane">
        <div class="promo-button-wrap">
            <span>Whats New?</span>
            <p>Diskon 10%
                <span class="teks-prom">Seluruh Paket </span>
            </p>

            <p class="btn-prom">Selengkapnya</p>
        </div>
    </div>
</div>
<!--modal-->
<div class="modal fade modal-pane" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            ...
        </div>
    </div>
</div>




<script src="<?php echo base_url() ?>assets/js/jquery-2.1.4.min.js"></script>   
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>   
<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.js"></script>   
<script src="<?php echo base_url() ?>assets/js/moment.min.js"></script> 
<script src="<?php echo base_url() ?>assets/js/fullcalendar.min.js"></script> 
<script src="<?php echo base_url() ?>assets/js/lang-all.js"></script> 
<script src="<?php echo base_url() ?>assets/js/css3-animate-it.js"></script> 
<script src="<?php echo base_url() ?>assets/js/all.js"></script> 
<script src="<?php echo base_url() ?>assets/js/main.js"></script>   