<?php

class profil_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}

        
        
        
        
        public function _getLastUmroh($user_id) {
            
            $this->db->select('a.anggota_paket_id,a.bang_haji_code,a.paket_id,'
                    . 'a.user_id,a.status_lunas,a.status_berangkat,'
                    . 'b.paket_name,b.tanggal_berangkat,b.tanggal_pulang,b.harga_total_paket,b.discount,b.keterangan,'
                    . 'b.travel_agent_id, c.travel_agent_code, c.nama as travel_agent_nama,'
                    . 'c.alamat as travel_agent_alamat, c.telp as travel_agent_telp, c.email as travel_agent_email,'
                    . 'c.website as travel_agent_website, c.penanggung_jawab as travel_agent_penanggung_jawab, c.kode_desa as travel_agent_kode_desa');
            $this->db->from('anggota_paket a');
            $this->db->join('paket b','a.paket_id = b.paket_id');
            $this->db->join('travel_agent c','b.travel_agent_id = c.travel_agent_id');
            
            $this->db->where('a.user_id',$user_id);
            $this->db->order_by('tanggal_berangkat','DESC');
            
         
            $ress= $this->db->get()->row_array();
            
            return $ress;
            
        }
        
        public function _getActiveBangHajiCode($user_id) {
            $this->db->select('a.bang_haji_code');
            $this->db->from('anggota_paket a');
            $this->db->join('user b','a.user_id = b.user_id','LEFT');
            $this->db->where('a.user_id',$user_id);
            $this->db->where('a.status_lunas','1');
            $this->db->order_by('a.created_at DESC');
         
            $ress= $this->db->get()->row_array();
            
            return $ress;
        }
        
        public function _getFullProfile($user_id) {
            
            $this->db->select('*');
            $this->db->from('user a');
            $this->db->where('a.user_id',$user_id);
         
            $ress= $this->db->get()->row_array();
            
            return $ress;
            
        }
            
}
