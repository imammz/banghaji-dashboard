<?php

class Travel extends MX_Controller {

    function __construct() {
        parent:: __construct();
        //$this->checkLogin();
       $this->output->enable_profiler($this->debug);
        
    }

    var $pages = array(
        'modules' => 'user',
        'class' => 'travel',
        'function' => 'index'
    );
    
    var $debug = true;
    

    
    public function get_foto_gallery($token, $user_id, $anggota_paket_id) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
        
    }
    
    public function get_jadwal_kegiatan($token, $user_id, $anggota_paket_id) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
        
    }
    
    
    public function get_info_perjalanan($token, $user_id, $anggota_paket_id) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
        
    }
    
    public function get_info_pemesanan($token, $user_id, $anggota_paket_id) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
        
    }
    
    public function load_list_perjalanan($token, $user_id
            ) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
        
    }
    
  
   
    
    
    
}