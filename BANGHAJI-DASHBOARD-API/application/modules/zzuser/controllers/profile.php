<?php

class Profile extends MX_Controller {

    function __construct() {
        parent:: __construct();
        //$this->checkLogin();
       $this->output->enable_profiler($this->debug);
       $this->load->model('profil_model');
        
    }

    var $pages = array(
        'modules' => 'user',
        'class' => 'profile',
        'function' => 'index'
    );
    
    var $debug = true;
    

    public function get_last_umroh($token,$user_id) {
         // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         
        $this->pages['function'] = 'index';
        
        $profil_model = new profil_model();
       
        $data = array();
        $data['pages'] = $this->pages;
        $data['data'] = $profil_model->_getLastUmroh($user_id);
        
        
        $this->_outputjson($data);


        //$this->load->view('home_view', $data);
    }
    
    public function get_active_bang_haji_code($token,$user_id) {
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         


        $this->pages['function'] = 'index';
        
 
        
        $profil_model = new profil_model();
        
        $data = array();
        $data['pages'] = $this->pages;
        $data['data'] = $profil_model->_getActiveBangHajiCode($user_id);
        
        
        $this->_outputjson($data);
    }
    
    public function get_full_profile($token,$user_id) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
   
        $this->pages['function'] = 'get_full_profile';
        
 
        
        $profil_model = new profil_model();
        
        $data = array();
        $data['pages'] = $this->pages;
        $data['data'] = $profil_model->_getFullProfile($user_id);
        
        
        $this->_outputjson($data);
        
    }
    


}

?>