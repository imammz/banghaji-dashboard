<?php

class Payment extends MX_Controller {

    function __construct() {
        parent:: __construct();
        //$this->checkLogin();
       $this->output->enable_profiler($this->debug);
       $this->load->model('payment_model');
        
    }

    var $pages = array(
        'modules' => 'user',
        'class' => 'payment',
        'function' => 'index'
    );
    
    var $debug = true;
    

    
    public function load_payment_method($token) {
        
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         
        $this->pages['function'] = 'load_payment_method';
        
 
        
        $payment_model = new payment_model();
        
        $data = array();
        $data['pages'] = $this->pages;
        $data['data'] = $payment_model->paymentMethod();
        
        
        $this->_outputjson($data);
         
        
    }
    
    
    // remember latter
    public function get_payment_profile($token, $user_id ) {
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
    }
   
    
    
    
}