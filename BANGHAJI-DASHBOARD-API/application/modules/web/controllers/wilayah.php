<?php

class Wilayah extends MX_Controller {

    function __construct() {
        parent:: __construct();
        //$this->checkLogin();
       // Token = NBS100amb;
        
    }

    var $pages = array(
        'modules' => 'web',
        'class' => 'master',
        'function' => 'index'
    );
    
    

   public function loadProvinsi($token) {
       
       // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
     
       $this->db->select("ref_wilayah_id,kode_negara,kode_prov,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 2 AND kode_negara = '62'");
       $ress = $this->db->get()->result_array();
       
       $data= array();
       $data['result'] = (count($ress)>=0)?'200':'500'; 
       $data['message'] = (count($ress)>=0)?'Success':'Error'; 
       $data['count'] = count($ress);
       $data['data'] = (count($ress)>=0)?$ress:null;
       
       $this->_outputjson($data);
   }
   
   public function loadKabKotaByKodeProv($token,$kode_prov) {
       
       // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
     
       $this->db->select("ref_wilayah_id,kode_negara,kode_prov,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 2 AND kode_negara = '62' AND kode_prov = '$kode_prov'"); 
       $prov = $this->db->get()->row_array();
       
       $this->db->select("ref_wilayah_id,kode_kab,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 3 AND kode_negara = '62' AND kode_prov = '$kode_prov'");
       $prov['kab_kota'] = $this->db->get()->result_array();
       
       $ress = $prov;
       
       $data= array();
       $data['result'] = (count($ress)>=0)?'200':'500'; 
       $data['message'] = (count($ress)>=0)?'Success':'Error'; 
       $data['count'] = count($ress['kab_kota']);
       $data['data'] = (count($ress)>=0)?$ress:null;
       
       $this->_outputjson($data);
   }
   
   public function loadKecByKodeKabKota($token,$kode_kab_kota) {
       
       // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         
       $this->db->select("ref_wilayah_id,kode_negara,kode_prov,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 2 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kab_kota, 0, 2)."'"); 
       $prov = $this->db->get()->row_array();  
     
       $this->db->select("ref_wilayah_id,kode_kab,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 3 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kab_kota, 0, 2)."' "
               . "AND kode_kab = '$kode_kab_kota'");
       $prov['kab_kota'] = $this->db->get()->row_array();
       

       $this->db->select("ref_wilayah_id,kode_kec,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 4 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kab_kota, 0, 2)."' "
               . "AND kode_kab = '$kode_kab_kota'");
       $prov['kab_kota']['kec'] = $this->db->get()->result_array();
       
       $ress = $prov;
       
       $data= array();
       $data['result'] = (count($ress)>=0)?'200':'500'; 
       $data['message'] = (count($ress)>=0)?'Success':'Error'; 
       $data['count'] = count($ress['kab_kota']);
       $data['data'] = (count($ress)>=0)?$ress:null;
       
       $this->_outputjson($data);
   }
   
   
   public function loadKelByKodeKec($token,$kode_kec) {
       
       // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         
       $this->db->select("ref_wilayah_id,kode_negara,kode_prov,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 2 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kec, 0, 2)."'"); 
       $prov = $this->db->get()->row_array();  
     
       $this->db->select("ref_wilayah_id,kode_kab,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 3 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kec, 0, 2)."' "
               . "AND kode_kab = '".substr($kode_kec, 0, 4)."'");
       $prov['kab_kota'] = $this->db->get()->row_array();
       

       $this->db->select("ref_wilayah_id,kode_kec,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 4 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kec, 0, 2)."' "
               . "AND kode_kab = '".substr($kode_kec, 0, 4)."' "
               . "AND kode_kec = '$kode_kec'");
       $prov['kab_kota']['kec'] = $this->db->get()->row_array();
       
       
       $this->db->select("ref_wilayah_id,kode_desa,level_wilayah,nama_wilayah");
       $this->db->from("ref_wilayah");
       $this->db->where("level_wilayah = 5 AND kode_negara = '62' "
               . "AND kode_prov = '".  substr($kode_kab_kota, 0, 2)."' "
               . "AND kode_kab = '".substr($kode_kec, 0, 4)."' "
               . "AND kode_kec = '$kode_kec'");
       $prov['kab_kota']['kec'] = $this->db->get()->result_array();
       
       $ress = $prov;
       
       $data= array();
       $data['result'] = (count($ress)>=0)?'200':'500'; 
       $data['message'] = (count($ress)>=0)?'Success':'Error'; 
       $data['count'] = count($ress['kab_kota']);
       $data['data'] = (count($ress)>=0)?$ress:null;
       
       $this->_outputjson($data);
   }
   
   


}

?>