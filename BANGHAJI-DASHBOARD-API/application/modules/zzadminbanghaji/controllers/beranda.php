<?php

class Beranda extends MX_Controller {

    function __construct() {
        parent:: __construct();
        //$this->checkLogin();
        // Token = NBS100amb;
        
    }

    var $pages = array(
        'modules' => 'beranda',
        'class' => 'beranda',
        'function' => 'index'
    );
    
    

    public function get_last_umroh($token,$user_id) {
         // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         
         
        $this->output->enable_profiler(false);
        $this->pages['function'] = 'index';
        
        $this->load->model('profil/profil_model');
        
        $profil_model = new profil_model();
        
        $data = array();
        $data['pages'] = $this->pages;
        $data['last_umroh'] = $profil_model->_getLastUmroh($user_id);
        
        
        $this->_outputjson($data);


        //$this->load->view('home_view', $data);
    }
    
    public function get_active_bang_haji_code($token,$user_id) {
        // wajib
         $_SESSION['token'] = $token; 
         $this->check_token();
        //
         
         
        $this->output->enable_profiler(false);
        $this->pages['function'] = 'index';
        
        $this->load->model('profil/profil_model');
        
        $profil_model = new profil_model();
        
        $data = array();
        $data['pages'] = $this->pages;
        $data['bang_haji_code'] = $profil_model->_getLastUmroh($user_id);
        
        
        $this->_outputjson($data);
    }
    


}

?>